from django.conf.urls import url
from . import views

app_name = 'board'

urlpatterns = [
    url(r'^$', views.index),
    url(r'^boardList/$', views.FreeBoardList.as_view()),
    url(r'^boardList/write', views.FreeBoardList.as_view()),
    # url(r'^boardList/Search/$', views.FreeBoardSearch.as_view()),
    # url(r'^boardList/freeboard$', views.FreeBoardList.as_view()),
    # url(r'^boardList/', views.FreeBoardList.as_view()),
    url(r'^boardList/(?P<pk>[0-9]+)/$', views.FreeBoardDetail.as_view()),
    url(r'^comments/$', views.CommentDetail.as_view()),
    url(r'^commentrecommend/$', views.commentsLike),
    url(r'^([0-9]+)/$', views.index),
    url(r'^write/$', views.index),
]
