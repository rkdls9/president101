from django.db import models
from hitcount.models import HitCountMixin
from vote.managers import VotableManager


class FreeBoard(models.Model, HitCountMixin):
    title = models.CharField(max_length=1024)
    body = models.TextField()
    author = models.ForeignKey('auth.User', related_name='freeboard', on_delete=models.CASCADE)
    regdate = models.DateTimeField(auto_created=True, auto_now_add=True)

    class Meta:
        ordering = ["-pk"]


class Comment(models.Model):
    post = models.ForeignKey(FreeBoard, on_delete=models.CASCADE, related_name='FreeBoardComment')

    # title = models.CharField(max_length=200, null=False, default='empty')
    # author = models.CharField(max_length=200)
    author = models.ForeignKey('auth.User', related_name='freeboardComment', on_delete=models.CASCADE)
    password = models.CharField(max_length=20, null=False, default='empty')

    created_date = models.DateTimeField(auto_created=True, auto_now=True)
    # created_date = models.DateTimeField(auto_created=True, default=timezone.now)
    text = models.TextField()
    votes = VotableManager()

    def __str__(self):
        return self.post.__str__() + self.text.__str__()
