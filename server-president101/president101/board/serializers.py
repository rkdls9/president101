from rest_framework import serializers
from .models import FreeBoard, Comment


class CommentSerializer(serializers.ModelSerializer):
    like = serializers.SerializerMethodField()
    author = serializers.ReadOnlyField(source='author.username')

    class Meta:
        model = Comment
        # fields = ('post', 'author', 'created_date', 'text', 'pk')
        fields = ('author', 'password', 'created_date', 'text', 'pk', 'like')

    def get_like(self, obj):
        return obj.votes.count()


class FreeBoardSerializer(serializers.ModelSerializer):
    author = serializers.ReadOnlyField(source='author.username')
    views = serializers.SerializerMethodField()

    class Meta:
        model = FreeBoard
        fields = ('title', 'regdate', 'pk', 'author', 'views', 'body')

    def get_views(self, obj):
        return FreeBoard.objects.get(pk=obj.id).hit_count.hits


class FreeBoardDetailSerializer(serializers.ModelSerializer):
    FreeBoardComment = CommentSerializer(many=True)
    views = serializers.SerializerMethodField()
    author = serializers.ReadOnlyField(source='author.username')

    class Meta:
        model = FreeBoard
        fields = ('title', 'regdate', 'body', 'pk', 'author', 'views', 'FreeBoardComment')

    def get_views(self, obj):
        return FreeBoard.objects.get(pk=obj.id).hit_count.hits