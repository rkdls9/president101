import logging
from django.contrib.auth.models import Permission, User
from django.core.exceptions import ObjectDoesNotExist
from django.http import JsonResponse
from django.shortcuts import render
from hitcount.models import HitCount
from collections import OrderedDict
from hitcount.views import HitCountMixin
from rest_framework import filters
from rest_framework import generics
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.generics import ListAPIView
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from .serializers import FreeBoardSerializer, FreeBoardDetailSerializer, CommentSerializer
from .models import FreeBoard, Comment

logger = logging.getLogger(__name__)


def index(request, *args):
    return render(request, 'index.html')


class StandardResultsSetPagination(PageNumberPagination):
    page_size = 10
    page_query_param = 'page'
    page_size_query_param = 'page_size'
    max_page_size = 100


class FreeBoardList(generics.ListCreateAPIView):
    queryset = FreeBoard.objects.all()
    serializer_class = FreeBoardSerializer
    pagination_class = StandardResultsSetPagination
    filter_backends = (filters.SearchFilter,)
    search_fields = ('title', 'body')

    def get_queryset(self):
        return FreeBoard.objects.all()

    def get(self, request, *args, **kwargs):
        return self.list(request, *args, **kwargs)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        # headers = self.get_success_headers(serializer.data)
        return self.list(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.save(author_id=self.request.user.id)


class FreeBoardSearch(ListAPIView):
    serializer_class = FreeBoardSerializer

    def get_queryset(self):
        title = self.request.GET['title']
        return FreeBoard.objects.filter(title=title)


class FreeBoardDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = FreeBoard.objects.all()
    serializer_class = FreeBoardDetailSerializer

    def get(self, request, *args, **kwargs):
        instance = self.get_object()
        hit_count = HitCount.objects.get_for_object(instance)
        HitCountMixin.hit_count(request, hit_count)
        serializer = self.get_serializer(instance)
        return Response(serializer.data)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        if instance.author == request.user:
            self.perform_destroy(instance)
            return Response(status=status.HTTP_200_OK)
        else:
            return Response(status=status.HTTP_401_UNAUTHORIZED)


class CommentDetail(generics.ListCreateAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    # permission_classes = [Permission.IsAdminOrIsSelf]

    def perform_create(self, serializer):
        freInstance = FreeBoard.objects.get(pk=self.request.data['freePk'])
        serializer.save(author_id=self.request.user.id, post=freInstance)


class CommentUpdate(generics.RetrieveUpdateDestroyAPIView):
    queryset = Comment.objects.all()
    serializer_class = CommentSerializer

    def partial_update(self, request, *args, **kwargs):
        print(request)


@api_view(['UPDATE'])
def commentsLike(request):
    print(request.data)
    if request.method == 'UPDATE':
        comment = Comment.objects.get(pk=request.data['pk'])
        # tobe fix 댓글 추천하면 전체 댓글을 다시 렌더링하는거 수정해야함.
        try:
            voter = User.objects.get(username=request.user)
        except ObjectDoesNotExist:
            print('유저가 없습니다.')
            message = '로그인한 유저만 좋아할수있습니다.'
            return Response({'message': message}, status=status.HTTP_200_OK)
        if request.data['like'] == 'like':
            result = comment.votes.up(voter.id)
        elif request.data['like'] == 'dislike':
            result = comment.votes.down(voter.id)
        else:
            message = '좋아요, 싫어요 둘중하나를 선택해주세요.'
            return Response({'message': message}, status=status.HTTP_200_OK)
        if result:
            print('성공')
            return Response({'message': '성공'}, status=status.HTTP_200_OK)
        message = '이미 좋아하였습니다.'
        print(message)
        return Response({'message': message}, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)
