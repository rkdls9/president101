import time
import logging

import datetime

from allauth.account.views import SignupView, LoginView
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter
from allauth.socialaccount.models import SocialAccount
from allauth.socialaccount.providers.facebook.views import FacebookOAuth2Adapter
from django.contrib import auth
from django.contrib.auth.models import User
# from django.core.cache import cache
from django.core.cache import cache
from django.core.exceptions import ObjectDoesNotExist
from django.core.signals import request_started, request_finished
from django.shortcuts import render
# from rest_auth.registration.views import SocialLoginView
from rest_framework import generics
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .serializers import UserSerializer, PresidentSerializer, ThistimeCommentSerializer, ThisTimePresidentSerializer, \
    MyPageSerializer, ThisMonthPresidentSerializer
from .models import President, ThisTime, ThisTimePresident, Comment
from django.conf import settings

logger = logging.getLogger(__name__)


def index(request, *args):
    return render(request, 'index.html')


@api_view(['POST'])
def validate(request):
    serializer = UserSerializer(request.user)
    weekth = datetime.datetime.now().isocalendar()[1]

    try:
        this_president = ThisTime.objects.get(times=weekth)
    except ObjectDoesNotExist:
        this_president = ThisTime.objects.create(times=weekth)
    serializerdata = {'data': serializer.data, 'voted': this_president.votes.exists(request.user.id)}
    return Response(serializerdata)


class PresidentList(generics.ListAPIView):
    queryset = President.objects.all()
    serializer_class = PresidentSerializer

    def get_queryset(self):
        return President.objects.all()

    def list(self, request, *args, **kwargs):
        use_cache = getattr(settings, 'CACHE_USE')
        if use_cache:
            data = cache.get('PresidentList')
            if data is None:
                queryset = self.filter_queryset(self.get_queryset())
                serializer = self.get_serializer(queryset, many=True)
                data = serializer.data
                logger.debug('프레지던트 리스트 캐시 안씀')
                cache.set('PresidentList', data)
        else:
            queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(queryset, many=True)
            data = serializer.data
            logger.debug('프레지던트 리스트 캐시 디폴트로 안씀')
        return Response(data)


class CommentList(generics.ListAPIView):
    queryset = Comment.objects.all()
    serializer_class = ThistimeCommentSerializer


@api_view(['UPDATE'])
def presidentLike(request):
    if request.method == 'UPDATE':
        # print("request.data['pk']", request.data['pk'])
        # print("request.data['thisTime']", request.data['thisTime'])
        weekth = datetime.datetime.now().isocalendar()[1]  # 몇번째 주인지 확인
        # datetime.datetime.now().isoweekday()
        president = President.objects.get(pk=request.data['pk'])
        try:
            thisTime = ThisTime.objects.get(times=weekth)
        except ObjectDoesNotExist:
            logger.debug('{}회차가 없어서 새로만듬.(새로운 한주의 시작, 첫번째 투표자)'.format(thisTime))
            thisTime = ThisTime.objects.create(times=weekth)
        try:
            thistimepresident = ThisTimePresident.objects.get(thisTime=thisTime, nameFor=president)
        except ObjectDoesNotExist:
            logger.debug('이번주에 투표한 대통령{} 가 없어서 새로만듬 thisTime {}'.format(president, thisTime))
            thistimepresident = ThisTimePresident.objects.create(thisTime=thisTime, nameFor=president)
        try:
            voter = User.objects.get(username=request.user)
        except ObjectDoesNotExist:
            logger.debug('유저가 없습니다.')
            message = '로그인한 유저만 좋아할수있습니다.'
            return Response({'message': message}, status=status.HTTP_200_OK)
        exists = thisTime.votes.exists(voter.id)
        if not exists:
            thistimeresult = thisTime.votes.up(voter.id)
            result = thistimepresident.votes.up(voter.id)
            logger.debug('thistimeresult: [{}]'.format(thistimeresult))
            if result:
                # serializer = PresidentSerializer(president)
                message = '투표되었습니다'
                return Response({'message': message}, status=status.HTTP_200_OK)
            message = '이미 대통령에 투표 하였습니다.'
            logger.debug(message)
            return Response({'message': message}, status=status.HTTP_200_OK)
        else:
            message = '다른 대통령에 이미 투표하였습니다.'
            logger.debug(message)
            return Response({'message': message}, status=status.HTTP_200_OK)
    return Response(status=status.HTTP_400_BAD_REQUEST)


class thisTimePresident(generics.ListAPIView):
    queryset = ThisTimePresident.objects.all()
    serializer_class = ThisTimePresidentSerializer

    def get_queryset(self):
        try:
            # 일요일에서 월요일로 넘어가는때 바뀜
            # ex) 3월5일 = 9, 3월6일 = 10
            weekth = datetime.datetime.now().isocalendar()[1]
            this_week = ThisTime.objects.get(times=weekth)
        except ObjectDoesNotExist:
            this_week = ThisTime.objects.create(times=weekth)
        this_week_president = ThisTimePresident.objects.filter(thisTime=this_week).select_related(
            'thisTime').select_related('nameFor')
        # this_week_president = ThisTimePresident.objects.filter(thisTime=this_week)
        return this_week_president

    def list(self, request, *args, **kwargs):
        use_cache = getattr(settings, 'CACHE_USE')
        if use_cache:
            data = cache.get('thistimes')
            if data is None:
                queryset = self.filter_queryset(self.get_queryset())
                serializer = self.get_serializer(queryset, many=True)
                data = serializer.data
                logger.debug('이번주 대통령 투표.. 캐시안쓰고 보여줌')
                cache.set('thistimes', data)
        else:
            queryset = self.filter_queryset(self.get_queryset())
            serializer = self.get_serializer(queryset, many=True)
            data = serializer.data
            logger.debug('이번주 대통령 투표.. 캐시디폴트로 안씀')
        return Response(data)


class MyPage(generics.ListAPIView):
    queryset = ThisTimePresident.objects.all()
    serializer_class = MyPageSerializer

    def get_queryset(self):
        return ThisTimePresident.votes.all(self.request.user.id)


class MonthPresident(generics.ListAPIView):
    queryset = ThisTime.objects.all()
    serializer_class = ThisMonthPresidentSerializer

    def get_queryset(self):
        this_week_president = ThisTime.objects.all().order_by('-times')
        return this_week_president

    '''테스트케이스 작성 test case'''

    def list(self, request, *args, **kwargs):
        use_cache = getattr(settings, 'CACHE_USE')
        if use_cache:
            data = cache.get('thisMonth')
            if data is None:
                queryset = self.filter_queryset(self.get_queryset())

                page = self.paginate_queryset(queryset)
                if page is not None:
                    serializer = self.get_serializer(page, many=True)
                    return self.get_paginated_response(serializer.data)

                serializer = self.get_serializer(queryset, many=True)
                data = serializer.data
                logger.debug('MonthPresident 캐시안씀')
                cache.set('thisMonth', data)
                # serializer_time = time.time() - serializer_start
        else:
            queryset = self.filter_queryset(self.get_queryset())

            page = self.paginate_queryset(queryset)
            if page is not None:
                serializer = self.get_serializer(page, many=True)
                return self.get_paginated_response(serializer.data)

            serializer = self.get_serializer(queryset, many=True)
            data = serializer.data
            logger.debug('MonthPresident 캐시 디폴트로 안씀')
        return Response(data)


class MySignupView(SignupView):
    template_name = 'index.html'


class MyLoginView(LoginView):
    template_name = 'index.html'


class MyACCOUNTADAPTOR(DefaultSocialAccountAdapter):
    def get_signup_form_initial_data(self, sociallogin):
        user = sociallogin.user
        sociallogin.email_addresses[0].email
        logger.debug('user: {} self: {}'.format(user, self))

    def get_loginredirect_rul(self, request):
        to = '/'
        logger.debug(('login Retirect To {}'.format(to)))
        return to
