import datetime
from allauth.socialaccount.models import SocialAccount
from django.db.models import Count
from rest_framework import serializers
from django.contrib.auth.models import User
from .models import President, Comment, Articles, ThisTimePresident, ThisTime


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'first_name', 'last_name')


class ThistimeCommentSerializer(serializers.ModelSerializer):
    like = serializers.SerializerMethodField()
    author = serializers.ReadOnlyField(source='author.username')

    class Meta:
        model = Comment
        fields = ('author', 'created_date', 'text', 'like', 'pk')

    def get_like(self, obj):
        return obj.votes.count()


class ArticlesSerializer(serializers.ModelSerializer):
    like = serializers.SerializerMethodField()

    class Meta:
        model = Articles
        fields = ('presidentFor', 'headLine', 'articleLink', 'articleSource', 'articleDate', 'getDate', 'like',)

    def get_like(self, obj):
        return obj.votes.count()


class ThisTimePresidentSerializer(serializers.ModelSerializer):
    like = serializers.SerializerMethodField()
    # nameFor = serializers.SlugRelatedField(read_only=True, slug_field='President.Name')
    # nameFor = serializers.ReadOnlyField(source='President__Name')
    thisTime = serializers.StringRelatedField(source='thisTime.times')
    nameFor = serializers.StringRelatedField(source='nameFor.Name')
    users = serializers.SerializerMethodField()
    nextUpdate = serializers.SerializerMethodField()

    class Meta:
        model = ThisTimePresident
        fields = ('thisTime', 'nameFor', 'like', 'users', 'nextUpdate')

    def get_like(self, obj):
        return obj.votes.count()

    def get_nextUpdate(self, obj):
        return format(datetime.datetime.now() + datetime.timedelta(minutes=10), '%H:%M:%S')

    # tobe fix 고쳐야함.. 사람많아지면 엄청 느려질듯!!
    # def get_users(self, obj):
    #     user_ids = [x[0] for x in obj.votes.user_ids()]
    #     # female = \
    #     #     SocialAccount.objects.filter(provider='facebook', user_id__in=user_ids) \
    #     #         .filter(extra_data__contains='"gender": "female"').count()
    #     # male = len(user_ids) - female
    #     users = SocialAccount.objects.filter(provider='facebook', user_id__in=user_ids)
    #     age_range = {}
    #     female = 0
    #     for i, val in enumerate(users):
    #         # age_range.append(val.extra_data['age_range'])
    #         try:
    #             age_range[val.extra_data['age_range']['min']]
    #         except KeyError:
    #             age_range[val.extra_data['age_range']['min']] = 0
    #         age_range[val.extra_data['age_range']['min']] += 1
    #         if val.extra_data['gender'] == 'female':
    #             female += 1
    #     male = len(user_ids) - female
    #     # SocialAccount.objects.filter(provider='facebook', user_id__in=user_ids)[0].extra_data['age_range']
    #     gender = {'female': female, 'male': male, 'age_range': age_range}
    #     return gender
    def get_users(self, obj):
        user_ids = [x[0] for x in obj.votes.user_ids()]
        # female = \
        #     SocialAccount.objects.filter(provider='facebook', user_id__in=user_ids) \
        #         .filter(extra_data__contains='"gender": "female"').count()
        # male = len(user_ids) - female
        users = SocialAccount.objects.filter(provider='facebook', user_id__in=user_ids)
        female = 0
        for i, val in enumerate(users):
            # age_range.append(val.extra_data['age_range'])
            if val.extra_data['gender'] == 'female':
                female += 1
        male = len(user_ids) - female
        # SocialAccount.objects.filter(provider='facebook', user_id__in=user_ids)[0].extra_data['age_range']
        gender = {'female': female, 'male': male}
        return gender


class PresidentSerializer(serializers.ModelSerializer):
    # PresidentComment = ThistimeCommentSerializer(many=True)
    PresidentArticle = ArticlesSerializer(many=True)

    # thisTimePresident = ThisTimePresidentSerializer(many=True)

    class Meta:
        model = President
        fields = (
            'Name', 'age', 'belongtobe', 'hometown', 'school', 'familly',
            'PresidentArticle', 'pk', 'numbers')


class ThisTimeSerializer(serializers.ModelSerializer):
    comment = ThistimeCommentSerializer(many=True)

    class Meta:
        model = President
        fields = ('nameFor', 'comment', 'times')


class MyPageSerializer(serializers.ModelSerializer):
    # this_week_president = serializers.SerializerMethodField()

    thisTime = serializers.StringRelatedField(source='thisTime.times')
    nameFor = serializers.StringRelatedField(source='nameFor.Name')

    class Meta:
        model = ThisTimePresident
        fields = ('thisTime', 'nameFor')

        # def get_this_week_president(self, obj):
        #     print('obj', obj.nameFor)
        #     return ThisTimePresident.votes.all(self.request.user.id)


class ThisMonthPresidentSerializer(serializers.ModelSerializer):
    like = serializers.SerializerMethodField()
    thistimevote = serializers.SerializerMethodField()
    nextUpdate = serializers.SerializerMethodField()

    class Meta:
        model = ThisTime
        fields = ('times', 'like', 'thistimevote', 'nextUpdate')

    def get_like(self, obj):
        return obj.votes.count()

    def get_thistimevote(self, obj):
        thisTiempresident = ThisTimePresident.objects.filter(thisTime=obj.id)
        data = []

        for i, val in enumerate(thisTiempresident):
            presidentName = val.nameFor.Name
            votes = val.votes.count()
            user_ids = [x[0] for x in val.votes.user_ids()]
            female = SocialAccount.objects.filter(provider='facebook', user_id__in=user_ids). \
                filter(extra_data__contains='"gender": "female"').count()
            male = len(user_ids) - female
            innserdata = {'nameFor': presidentName, 'like': votes, 'female': female, 'male': male}
            data.append(innserdata)
        # gender = {'female': female, 'male': male}
        return data

    def get_nextUpdate(self, obj):
        return format(datetime.datetime.now() + datetime.timedelta(minutes=10), '%H:%M:%S')