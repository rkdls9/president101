from django.db import models
from vote.managers import VotableManager


class President(models.Model):
    Name = models.CharField(max_length=20, null=False)
    age = models.IntegerField(default=1)
    belongtobe = models.CharField(max_length=50)
    hometown = models.CharField(max_length=100)
    school = models.CharField(max_length=50)
    familly = models.CharField(max_length=50)
    numbers = models.IntegerField(default=1)

    def __str__(self):
        return self.Name.__str__()


class ThisTime(models.Model):
    times = models.IntegerField()
    votes = VotableManager()

    def __str__(self):
        return self.times.__str__() + '회차'


class ThisTimePresident(models.Model):
    thisTime = models.ForeignKey(ThisTime, on_delete=models.CASCADE, related_name='thisTime')
    nameFor = models.ForeignKey(President, on_delete=models.CASCADE, related_name='thisTimePresident')
    votes = VotableManager()

    def __str__(self):
        return self.thisTime.__str__() + '대통령: '+self.nameFor.__str__()


class Articles(models.Model):
    presidentFor = models.ForeignKey(President, on_delete=models.CASCADE, related_name='PresidentArticle')
    headLine = models.CharField(max_length=200, null=False, default='기사헤드라인')
    articleLink = models.CharField(max_length=200, null=False)
    articleSource = models.CharField(max_length=50, null=False)
    articleDate = models.DateTimeField(verbose_name='기사 작성날짜', auto_created=False)
    getDate = models.DateTimeField(auto_created=True, auto_now=True)
    votes = VotableManager()

    def __str__(self):
        return self.headLine + ' ' + self.articleSource.__str__()


class Comment(models.Model):
    presidentFor = models.ForeignKey(President, on_delete=models.CASCADE, related_name='PresidentComment')
    thisTimeFor = models.ForeignKey(ThisTime, on_delete=models.CASCADE, related_name='ThisTimeComment')
    password = models.CharField(max_length=20, null=False, default='0000')
    created_date = models.DateTimeField(auto_created=True, auto_now=True)
    text = models.TextField()
    votes = VotableManager()
    author = models.ForeignKey('auth.User', related_name='userComment', on_delete=models.CASCADE)

    def __str__(self):
        return self.author.__str__() + ':' + self.text.__str__()
