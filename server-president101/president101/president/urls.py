from django.conf.urls import url
from . import views

app_name = 'president'

urlpatterns = [
    url(r'^$', views.index),
    url(r'^accounts/login/', views.MyLoginView.as_view(), name='account_login'),
    url(r'^accounts/signup/', views.MySignupView.as_view(), name='account_signup'),
    url(r'^accounts/email/', views.MySignupView.as_view(), name='account_email'),
    url(r'^accounts/social/signup/', views.MySignupView.as_view(), name='account_signup'),
    url(r'^validate', views.validate),
    url(r'^president', views.PresidentList.as_view()),
    url(r'^comment', views.CommentList.as_view()),
    url(r'^thisTimePresident/', views.thisTimePresident.as_view()),
    url(r'^thisMonthPresident/', views.MonthPresident.as_view()),
    url(r'^mypage/$', views.MyPage.as_view()),
    # url(r'^mypage*', views.index),
    # url(r'^login', views.index),
    # url(r'^rank', views.index),
    url(r'^contact', views.index),
    url(r'^_=_', views.index),
    url(r'^vote', views.presidentLike),
]

# urlpatterns += router.urls
