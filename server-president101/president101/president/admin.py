from django.contrib import admin
from .models import President, Comment, Articles, ThisTime, ThisTimePresident

admin.site.register(President)
admin.site.register(Articles)
admin.site.register(Comment)
admin.site.register(ThisTime)
admin.site.register(ThisTimePresident)


# Register your models here.
