from django.apps import AppConfig


class VotermanagerConfig(AppConfig):
    name = 'votermanager'
