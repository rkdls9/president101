/**
 * Created by youngil on 2017-02-07.
 */
import update from 'react-addons-update'
const initialstate = {
    comics: '',
    comic: {
        book: '',
        subtitles: [],
        AWS_image_files: {
            receive_complete:false,
            file_list:[],
        },
        drop_image_src: {
            receive_complete:false,
            src_list:[]
        },
    },
    comment:'',
};


const AWS = (state = initialstate, action) => {
    // console.log('inthe AWS Reducer action.type : ', action.type, ' action.data :', action);

    switch (action.type) {
        case 'comics_aws':
            return {
                ...state,
                comics: action.comics,
                comic: {
                    ...state.comic,
                    book: action.comics
                }
            };
        case 'subtitles_aws':
            return {
                ...state,
                comic: {
                    ...state.comic,
                    subtitles: action.subtitles
                },
            };

        case 'AWS_image_files':
            return {
                ...state,
                comic: {
                    ...state.comic,
                    AWS_image_files: {
                        ...state.comic.AWS_image_files,
                        receive_complete:true,
                        file_list:action.AWS_image_files,
                    }
                }
            };
        case 'drop_image_src':
            return {
                ...state,
                comic: {
                    ...state.comic,
                    drop_image_src: {
                        ...state.comic.drop_image_src,
                        receive_complete:true,
                        src_list:action.drop_image_src
                    }
                },
            };
        case 'comment':
            return update(state, {
                comment:{$set:action.comment}
            });
        case 'clearComments':
            return {
                ...state,
                comic: {
                    ...state.comic,
                    comment:[],
                }
            };
        case 'clear':
            console.log('clear starting');
            return{
                ...state,
                // state:undefined
                comic: {
                    ...state.comic,
                    drop_image_files: {
                        ...state.comic.drop_image_files,
                        receive_complete:false,
                        file_list:[],
                    },
                    drop_image_src: {
                        ...state.comic.drop_image_src,
                        receive_complete:false,
                        src_list:[],
                    }
                },
            };
        default:
            return state
    }
};
export default AWS