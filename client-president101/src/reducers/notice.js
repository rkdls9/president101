/**
 * Created by youngil on 2017-02-07.
 */
import update from 'react-addons-update'
const initialstate = {
    notice: '',
    FreeBoard: [],
    FreeBoardDetail: '',
    Loading: 'INIT',
    writeStatus: '',
    writedPk: '',
    commentWriteSuccess: '',
    Pagination: {
        count: '',
        next: '',
        previous: '',
    },
    LoadingBoardSearch:''
};
const notice = (state = initialstate, action) => {
    console.log('action.type',action.type);

    switch (action.type) {
        case 'notice':
            return update(state, {
                notice: {$set: action.notice}
            });

        case 'FreeBoard':
            return update(state, {
                FreeBoard: {$set: action.board},
                Pagination:{
                    count:{$set:action.count},
                    next:{$set:action.next},
                    previous:{$set:action.previous}
                }
            });
        case 'FreeBoardDetail':
            return update(state, {
                FreeBoardDetail: {$set: action.FreeBoardDetail},
            });
        case 'clearFreeBoardDetail':
            return update(state, {
                FreeBoardDetail: {$set: ''},
                Loading: {$set: 'INIT'},
            });
        case 'Loading':
            return update(state, {
                Loading: {$set: 'Loading'}
            });
        case 'LoadingDone':
            return update(state, {
                Loading: {$set: 'DONE'}
            });
        case 'FreeBoardWrite':
            return update(state, {
                writeStatus: {$set: action.status},
                writedPk: {$set: action.writedPk}
            });
        case 'clearWritingProps':
            return update(state, {
                writeStatus: {$set:''},
                writedPk: {$set: ''}
            });
        case 'FreeBoardDetailRefresh':
            return update(state, {
                commentWriteSuccess: {$set: 'success'},
            });
        case 'LoadingBoardSearch':
            return update(state, {
                LoadingBoardSearch: {$set: 'LoadingBoardSearch'}
            });
        case 'LoadingBoardSearchDone':
            return update(state, {
                LoadingBoardSearch: {$set: 'LoadingBoardSearchDone'}
            });
        default:
            return state
    }
};
export default notice