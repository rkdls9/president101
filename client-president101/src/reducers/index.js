/**
 * Created by youngil on 2017-02-07.
 */
import {combineReducers} from 'redux';
import notice from './notice';
import AWS from './AWS'
import authentication from './authentication'
import President from './President'
// import DropBox from './DropBox';

const presidentApp = combineReducers({
    notice,AWS,authentication,President
});

export default presidentApp