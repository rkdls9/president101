/**
 * Created by youngil on 2017-02-09.
 */
import update from 'react-addons-update'

const initialstate = {
    // President:{
        // name:'',
        // belongtobe:'',
        // age:'',
        // hometown:'',
        // school:'',
        // familly:'',
    // }
    President:[],
    Comment:[],
    ThisTimePresident:[],
    ThisMonthPresident:[],
    VotingRes:'',
    VOTERESULT:false,
};

const President = (state = initialstate, action) => {
    // console.log('inthe President Reducer action.type : ', action.type, ' action.data :', action);

    switch (action.type) {
        case 'getPresident':
            return update(state, {
                President: {$set: action.President}
            });
        case 'getPresidentFail':
            return update(state, {
                President: {$set:'정보를 가져오는데 실패하였습니다.'}
            });
        case 'getCommentList':
            return update(state, {
                Comment: {$set:action.comment}
            });
        case 'ThisTimePresident':
            return update(state, {
                ThisTimePresident: {$set : action.ThisTimePresident}
            });
        case 'ThisMonthPresident':
            return update(state, {
                ThisMonthPresident: {$set :action.ThisMonthPresident}
            });
        case 'VotingRes':
            return update(state, {
                VotingRes : {$set:action.VotingRes}
            });
        case 'VOTERESULT':
            return update(state, {
                VOTERESULT: {$set: action.VOTERESULT}
            });
        default:
            return state
    }
};
export default President