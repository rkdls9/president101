/**
 * Created by youngil on 2017-02-07.
 */
import update from 'react-addons-update'

const initialstate = {
    login: {
        status : 'INIT'
    },
    register: {
        status: 'INIT',
        error: -1
    },
    status: {
        valid: false,
        isLoggedIn: false,
        currentUser: '',
        pknum:'',
        message:'',
    },
    logindata: {
        first_name:'init',
        id:'0',
        last_name:'init',
        username:'init',
    },
    MyPage:[]
};

const authentication = (state = initialstate, action) => {
    // console.log('inthe authentication Reducer action.type : ', action.type, ' action.data :', action);
    switch (action.type) {
        case 'getStatus':
            return update(state, {
                status: {
                    isLoggedIn: {$set: true}
                }
            });
        case 'loginSuccess':
            return update(state, {
                login: {
                    status: { $set: 'SUCCESS'}
                },
                status: {
                    valid: { $set: true},
                    isLoggedIn: { $set: true},
                    currentUser: {$set: action.userdata.username},
                    pknum: {$set: action.userdata.id}
                }
            });
        case 'loginFail':
            return update(state, {
                login: {
                    status: { $set: 'FAIL'}
                },
                status: {
                    valid: {$set: false},
                    isLoggedIn: {$set: false},
                    message: {$set: action.message.responseText}
                }
            });

        case 'logOut':
            return update(state, {
                status:{
                    isLoggedIn: { $set: false },
                    currentUser: {$set: ''}
                }
            });
        case 'getUserData':
            return update(state, {
                logindata:{
                    first_name: {$set:action.first_name},
                    id: {$set:action.id},
                    last_name : {$set: action.last_name},
                    username: {$set: action.username},
                }
            });
        case 'MyPage':
            return update(state, {
                MyPage:{$set: action.MyPage}
            });
        default:
            return state
    }
};
export default authentication