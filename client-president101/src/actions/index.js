/**
 * Created by youngil on 2017-02-07.
 */
export const notice = (res) => {
    return {
        type: "notice",
        notice: res,
    }
};

export const AWS_image_files = (res) => {
    return {
        type: 'AWS_image_files',
        AWS_image_files: res,
    }
};
export const comics_aws = (res) => {
    return {
        type: 'comics_aws',
        comics: res
    }
};
export const subtitles_aws = (res) => {
    return {
        type: 'subtitles_aws',
        subtitles: res
    }
};

export const comment = (res) => {
    return {
        type: 'comment',
        comment: res
    }
};
export const clear = () => {
    return {
        type: 'clear'
    }
};
export const commentMessage = (res) => {
    return {
        type: 'commentMessage'
    }
};
export const FreeBoard = (res) => {
    return {
        type: 'FreeBoard',
        FreeBoard: res,
    }
};
export const FreeBoardDetail = (res) => {
    return {
        type: 'FreeBoardDetail',
        FreeBoardDetail: res,
    }
};

export const clearWritingProps = () =>{
    return {
        type: 'clearWritingProps',
    }
}
export const loginSuccess = (res) => {
    return {
        type: 'loginSuccess',
        userdata: res
    }
};
export const logOut = () => {
    return {
        type: 'logOut',
    }
};
export const loginFail = (res) => {
    return {
        type: 'loginFail',
        message: res
    }
};
export const clearComments = () => {
    return {
        type: 'clearComments',
    }
};
export const clearFreeBoardDetail = () => {
    return {
        type: 'clearFreeBoardDetail',
    }
};
export const Loading = () => {
    return {
        type: 'Loading',
    }
};
export const LoadingDone = () => {
    return {
        type: 'LoadingDone',
    }
};
export const LoadingBoardSearch = () =>{
    return {
        type:'LoadingBoardSearch',
    }
};
export const LoadingBoardSearchDone = () =>{
    return{
        type:'LoadingBoardSearchDone',
    }

}
export const VotingRes = (res) =>{
    return {
        type:'VotingRes',
        VotingRes:res
    }
};
export const getUserData = (res) => {
    return {
        type: 'getUserData',
        first_name: res.first_name,
        id: res.id,
        last_name: res.last_name,
        username: res.username
    }
};
export const VOTERESULT = (res) => {
    return {
        type:'VOTERESULT',
        VOTERESULT: res
    }
}
export const getPresident = (res) => {
    console.log('getPresident res', res);
    return {
        type: 'getPresident',
        President: res,
    }
};
export const getPresidentFail = (res) =>{
    return {
        type: 'getPresidentFail',
    }
};
export const getCommentList = (res) => {
    return {
        type: 'getCommentList',
        comment: res
    }
};
export const getBoardList = (res) => {
    return {
        type: 'FreeBoard',
        board : res.results,
        count: res.count,
        next: res.next,
        previous:res.previous
    }
};
export const FreeBoardWrite = (res) => {
    return{
        type:'FreeBoardWrite',
        status: 'success',
        writedPk: res
    }
};
export const FreeBoardDetailRefresh = (res) => {
    return {
        type: 'FreeBoardDetailRefresh',
    }
};
export const ThisTimePresident = (res) =>{
    return {
        type: 'ThisTimePresident',
        ThisTimePresident: res
    }
};

export const ThisMonthPresident = (res) => {
    return {
        type: 'ThisMonthPresident',
        ThisMonthPresident:res
    }
}
export const MyPage = (res) => {
    return {
        type: 'MyPage',
        MyPage: res
    }
}