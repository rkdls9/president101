/**
 * Created by youngil on 2017-02-07.
 */
import $ from 'jquery';
import Cookie from 'js-cookie'
import * as actions from './index';
let csrftoken = Cookie.get('csrftoken');

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}
$.ajaxSetup({
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
            xhr.setRequestHeader("X-CSRFToken", csrftoken);
        }
    }
});
// export const defaulturl = 'http://ec2.president101.com';
// export const defaulturl = 'http://127.0.0.1:8000';
export const defaulturl = '';


export const putComments = (...args) => {
    console.log('put args', args);
    // author,title,text,password
    const author = args[0];
    // const title = args[1];
    const text = args[1];
    const password = args[2];
    const pk = args[3];
    // const url = args[3];
    // const book = args[4];
    // const subtitle = args[5];

    return (dispatch) => {
        dispatch(actions.Loading());
        $.ajax({
            url: `${defaulturl}/board/comments/`,
            type: 'POST',
            data: {'author': author, 'text': text, 'password': password, 'freePk': pk},
            success: function (res) {
                console.log('prin put res', res);
                dispatch(actions.comment(res));
                dispatch(actions.LoadingDone());
            },
            error: function (status, res) {
                console.log('실패..', status, res);
            }
        });
    }
};

export const getComments = (pk) => {
    console.log('comment pk', pk);
    const titleurl = `${defaulturl}/board/comments/${pk}`;
    return (dispatch) => {
        dispatch(actions.Loading());
        $.ajax({
            url: titleurl,
            type: 'GET',
            // data: {'comic': comic, 'subtitle': subtitle},
            success: function (res) {
                console.log('comment res', res);
                // console.log('res', res.comment);
                dispatch(actions.comment(res));
                dispatch(actions.LoadingDone());
            },
            error: function (status, res) {
                console.log('2실패..', status, res);
                dispatch(actions.comment(' '));
                dispatch(actions.LoadingDone())
            }
        });
    }
};
export const getNotice = () => {
    return (dispatch) => {
        $.ajax({
            url: `${defaulturl}/api-notice`,
            type: 'GET',
            success: function (res) {
                console.log('res.notice', res);
                dispatch(actions.notice(res));
            },
            error: function (status, res) {
                console.log('공지사항 가져오기 실패..', status, res);
            }
        })
    }
};

export const recommend = (pk, like) => {
    let url = `${defaulturl}/board/commentrecommend/`;
    if (like === 'dislike') {
        url = `${defaulturl}/board/commentrecommend/`;
    }
    // url = url + '/' + pk+'/';
    console.log('pk, like', pk, url);
    return function (dispatch) {
        dispatch(actions.Loading());
        $.ajax({
            url: url,
            type: 'UPDATE',
            data: {'pk': pk, 'like': like},
            success: function (res, status) {
                console.log('추천하기 res', res, status);
                if (res) {
                    dispatch(actions.comment(res));
                    dispatch(actions.LoadingDone());
                } else {
                    dispatch(actions.commentMessage(res))
                }
            },
            error: function (status, res) {
                console.log('추천하기 실패 ', status, res);
                dispatch(actions.comment(res.comment));
            }
        });
    }
};

export const loginRequest = (username, password) => {
    return (dispatch) => {
        console.log('login start');
        $.ajax({
            url: `${defaulturl}/user/validate`,
            type: 'POST',
            data: {'id': username, 'password': password},
            success: function (res, status, jqXHR) {
                console.log('loginRequest', res, status, jqXHR.getResponseHeader('Set-Cookie'));
                console.log('loginRequest success', res, status);
                dispatch(actions.loginSuccess(res));

            }, error: function (res, status) {
                console.log('login fail', res, status);
                dispatch(actions.loginFail(res));
            }
        })
    }
};


export const LoginReq = () =>{
    return (dispatch) => {
        $.ajax({
            // url:`${defaulturl}/rest-auth/facebook/`,
            url:`${defaulturl}/accounts/facebook/login/`,
            type: 'POST',
            success: function (res, status, jqXHR) {
                console.log('after req', res, status, jqXHR.getResponseHeader('Set-Cookie'));
                console.log('after req success', res, status);
                dispatch(actions.loginSuccess(res));

            }, error: function (res, status) {
                console.log('after login fail', res, status);
                dispatch(actions.loginFail(res));
            }
        })
    }
}
export const login = (username, password) => {
    console.log('in the login');

    return (dispatch) => {
        dispatch(actions.Loading());
        $.ajax({
            url: `${defaulturl}/user/obtain-auth-token`,
            type: 'POST',
            data: {'username': username, 'password': password},
            success: function (res) {
                console.log('get Token', res);
                localStorage.token = res.token;
            }, error: function (status, res) {
                console.log('login fail', status, res)
            }

        })
    }
};


export const logOut = () => {
    console.log('logout start');
    return (dispatch) => {
        $.ajax({
            url: `${defaulturl}/accounts/logout/`,
            type: 'POST',
            success: function (res, status) {
                console.log('log out success', res, status);
                dispatch(actions.logOut())
            },
            error: function (status, res) {
                console.log('log out fail', status, res);
                dispatch(actions.logOut())
            }
        });

    }
};

export const clearComments = () => {
    return (dispatch) => {
        dispatch(actions.clearComments())
    }
};
export const clearFreeBoardDetail = () => {
    return (dispatch) => {
        dispatch(actions.clearFreeBoardDetail())
    }
};
export const getUserData = () => {
    return (dispatch) => {
        console.log('login start');
        $.ajax({
            url: `${defaulturl}/validate`,
            type: 'POST',
            // data:{'id':username, 'password':password},
            success: function (res) {
                console.log('loginRequest', res );
                dispatch(actions.getUserData(res.data));
                dispatch(actions.VOTERESULT(res.voted))

            }, error: function (res, status) {
                console.log('login fail', res, status);
                dispatch(actions.getUserDataFail(res));
            }
        })
    }
};
export const getPresident = () => {
    const date = new Date();
    const weekth = parseInt((6+ date.getDate()-date.getDay())/7)+1;
    const thisTime = `${date.getMonth()+1}${weekth}`;
    return (dispatch) => {
        dispatch(actions.Loading());
        $.ajax({
            url: `${defaulturl}/president/`,
            type: 'GET',
            data:{'thisTime':thisTime},
            success: function (res, status) {
                console.log('getPresident success', res, status);
                dispatch(actions.getPresident(res));
                dispatch(actions.LoadingDone());
            }, error: function (res, status) {
                console.log('getPresident error', res, status);
                dispatch(actions.getPresidentFail(res));
                dispatch(actions.LoadingDone());
            }
        })
    }
};
export const votePresident = (president) => {
    // const date = new Date();
    // const weekth = parseInt((6+ date.getDate()-date.getDay())/7)+1;
    // const thisTime = `${date.getMonth()+1}${weekth}`;

    return (dispatch) => {
        dispatch(actions.Loading());
        $.ajax({
            url: `${defaulturl}/vote`,
            type: 'UPDATE',
            data: {pk: president},
            success: function (res, status) {
                console.log('voting success', res, status);
                if (res.message) {
                    dispatch(actions.VotingRes(res.message));
                    dispatch(actions.VOTERESULT(true));
                }

                dispatch(actions.LoadingDone());
            }, error: function (res, status) {
                console.log('voting fail ', res, status);
                dispatch(actions.LoadingDone());
            }
        })
    }
};

export const commentList = () => {
    return (dispatch) => {
        dispatch(actions.Loading());
        $.ajax({
            url: `${defaulturl}/comment`,
            type: 'GET',
            success: function (res, status) {
                console.log('commentList success', res, status);
                dispatch(actions.getCommentList(res));
                dispatch(actions.LoadingDone());
            }, error: function (res, status) {
                console.log('commentList fail', res, status);
                dispatch(actions.getCommentList(res));
                dispatch(actions.LoadingDone());
            }
        })
    }
};
export const boardList = (url,page) => {
    let urlpath = url;
    const pageview = page;
    if (urlpath === 'page1') {
        urlpath = '/board/boardList'
    }
    console.log('paging', url, page);
    return (dispatch) => {
        dispatch(actions.Loading());
        $.ajax({
            url: `${defaulturl}${urlpath}/`,
            type: 'GET',
            data:{'page':pageview},
            success: function (res, status) {
                console.log('boardList success', res, status);
                dispatch(actions.getBoardList(res));
                dispatch(actions.LoadingDone());
            }, error: function (res, status) {
                console.log('boardList fail ', res, status);
                dispatch(actions.getBoardList(res));
                dispatch(actions.LoadingDone());
            }
        })
    }
};

export const boardListMore = (link) => {
    let urlpath = link;
    console.log('morList', urlpath);
    return (dispatch) => {
        dispatch(actions.LoadingBoardSearch());
        $.ajax({
            url: `${urlpath}`,
            type: 'GET',
            success: function (res, status) {
                console.log('boardListmore  success', res, status);
                dispatch(actions.getBoardList(res));
                dispatch(actions.LoadingBoardSearchDone());
            }, error: function (res, status) {
                console.log('boardList fail ', res, status);
                dispatch(actions.getBoardList(res));
                dispatch(actions.LoadingBoardSearchDone());
            }
        })
    }
};

export const searchBoardList = (url,page, keyword) => {
    let urlpath = url;
    const pageview = page;
    if (urlpath === 'page1') {
        urlpath = '/board/boardList'
    }
    console.log('paging', url, page);
    return (dispatch) => {
        dispatch(actions.Loading());
        $.ajax({
            url: `${defaulturl}/${urlpath}`,
            type: 'GET',
            data:{'page':pageview, 'search':keyword},
            success: function (res, status) {
                console.log('boardList success', res, status);
                dispatch(actions.getBoardList(res));
                dispatch(actions.LoadingDone());
            }, error: function (res, status) {
                console.log('boardList fail ', res, status);
                dispatch(actions.getBoardList(res));
                dispatch(actions.LoadingDone());
            }
        })
    }
};

export const boardDetail = (pk) => {
    return (dispatch) => {
        dispatch(actions.Loading());
        $.ajax({
            url: `${defaulturl}/board/boardList/${pk}/`,
            type: 'GET',
            success: function (res, status) {
                console.log('boardDetail scc', res, status);
                dispatch(actions.FreeBoardDetail(res));
                dispatch(actions.LoadingDone());
            }, error: function (res, status) {
                console.log('boardDetail faile', res, status);
                dispatch(actions.LoadingDone());
            }

        })
    }
};

export const writingBoard = (title, text, id) => {
    return (dispatch) => {
        dispatch(actions.Loading());
        $.ajax({
            url: `${defaulturl}/board/boardList/`,
            type: 'POST',
            data: {title: title, body: text},
            success: function (res, status) {
                console.log('boardwriteSuccess', res, status);
                // dispatch(actions.FreeBoardDetail(res));
                dispatch(actions.getBoardList(res));
                // dispatch(actions.FreeBoardWrite(res.pk));
                dispatch(actions.LoadingDone());
            }, error: function (res, status) {
                console.log('board write faile', res, status);
                dispatch(actions.FreeBoardWrite('fail'));
                dispatch(actions.LoadingDone());
            }
        })
    }
};

export const clearWritingProps = () =>{
    return(dispatch) =>{
        dispatch(actions.clearWritingProps())
    }
};
export const getThisTime = () =>{
    // const date = new Date();
    // const weekth = parseInt((6+ date.getDate()-date.getDay())/7)+1;
    // const thisTime = `${date.getMonth()+1}${weekth}`;
    // console.log(`thisTime number ${date.getMonth()+1}월 ${weekth}째 주`);
    return(dispatch) =>{
        dispatch(actions.Loading());
        $.ajax({
            url:`${defaulturl}/thisTimePresident/`,
            type:'GET',
            success: function (res, status) {
                console.log('thisTimegetSuccess', res, status);
                dispatch(actions.ThisTimePresident(res));
                dispatch(actions.LoadingDone())
            },error : function (res, status) {
                console.log('thisTimefail', res, status);
                dispatch(actions.LoadingDone())
            }
        })
    }
};
export const getThisMonth = () =>{
    return(dispatch) =>{
        dispatch(actions.Loading());
        $.ajax({
            url:`${defaulturl}/thisMonthPresident/`,
            type:'GET',
            success: function (res, status) {
                console.log('thisTimegetSuccess', res, status);
                dispatch(actions.ThisMonthPresident(res));
                dispatch(actions.LoadingDone())
            },error : function (res, status) {
                console.log('thisTimefail', res, status);
                dispatch(actions.LoadingDone())
            }
        })
    }
};
export const searchBoard = (text) => {
    console.log('Search start', text);
    return (dispatch) =>{
        dispatch(actions.LoadingBoardSearch());
        $.ajax({
            url:`${defaulturl}/board/boardList/`,
            type:'GET',
            data:{'search':text},
            success:function (res, status) {
                console.log('Search Success',res, status);
                dispatch(actions.getBoardList(res));
                dispatch(actions.LoadingBoardSearchDone());
            },error : function (res, status) {
                console.log('Search Error',res, status);
                dispatch(actions.LoadingBoardSearchDone());
            }
        })
    }
};

export const mypage = () => {
    return (dispatch)=>{
        dispatch(actions.Loading());
        $.ajax({
            url: `${defaulturl}/mypage/`,
            type: 'GET',
            success: function (res, status) {
                console.log('mypage', res, status);
                dispatch(actions.MyPage(res));
                dispatch(actions.LoadingDone());
            }, error: function (res, status) {
                console.log('mypage faile', res, status);
                dispatch(actions.MyPage(res));
                dispatch(actions.LoadingDone());
            }
        })
    }
}