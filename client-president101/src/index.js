import {createStore, applyMiddleware} from 'redux';
import reducers from './reducers/index';
import thunk from 'redux-thunk';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import ReactDOM from 'react-dom';
import React from 'react';
import {Provider} from 'react-redux';
// import {BrowserRouter as Router} from 'react-router-dom';
import {HashRouter as Router} from 'react-router-dom';
import Contents from './component/Contents'
import './App.css';
import '../node_modules/c3/c3.css' //이거 지우면안됨


const injectTapEventPlugin = require("react-tap-event-plugin");
injectTapEventPlugin();

const store = createStore(
    reducers,
    applyMiddleware(thunk)
);

// let dataa = JSON.parse(document.getElementById('initial-data').getAttribute('data-json'));
// let dataa = document.getElementById('initial-data').getAttribute('data-json');

const rootElement = document.getElementById('root');
ReactDOM.render(
    <MuiThemeProvider>
        <Provider store={store}>
            <Router>
                <Contents/>
            </Router>
        </Provider>
    </MuiThemeProvider>
    ,
    rootElement
);