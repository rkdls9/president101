/**
 * Created by youngil on 2017-02-15.
 */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {boardDetail, getComments, recommend, clearComments, putComments} from '../actions/DB'
import {CommentLayout} from './comment'
import CircularProgress from 'material-ui/CircularProgress'

class BoardDetail extends Component {

    constructor(props){
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    componentDidMount() {
        this.props.boardDetail(this.props.match.params.pk);
        // this.props.getComments(match.params.pk);
    }
    // shouldComponentUpdate(nextProps){
    //
    // }

    componentWillReceiveProps(nextProps){
        if (this.props.match.params.pk !== nextProps.match.params.pk){
            this.props.boardDetail(nextProps.match.params.pk);
            window.scrollTo(0, 0)
        }

        if (this.props.Comment !== nextProps.Comment){
            this.props.boardDetail(nextProps.match.params.pk);
        }
        // if (this.props.Loading !== nextProps.Loading){
        //     this.props.boardDetail(nextProps.match.params.pk);
        // }
    }
    componentWillUnmount(){
        this.props.clearComments()
    }

    handleSubmit(submittext) {
        // const url = '/comments/celestialNOTICE/' + this.props.match.params.pk;
        // const title = this.refs.title.value.trim();
        const text = submittext.trim();
        const password = "11".trim();
        // const {dispatch} = this.props;
        // console.log(url, author, this.props.params.celestialNOTICE);
        this.props.putComments(this.props.userdata.id, text, password, this.props.FreeBoardDetail.pk);
    }
    render() {
        console.log('BoardDetail ', this.props);
        const detailBoard= () =>(
            <div>
                <div
                    className="board_view_head"
                >
                    <ul>
                        <li
                            className="board_view_head_title"
                        >
                            <b>
                                {this.props.FreeBoardDetail['title']}
                            </b>
                        </li>
                        <li
                            className="board_author"
                        >
                            <p>by {this.props.FreeBoardDetail['author']}</p>
                        </li>
                        <li
                            className="etc"
                        >
                            <span>Hit {this.props.FreeBoardDetail['views']}</span>
                            <em>|</em>
                            <span>{this.props.FreeBoardDetail['regdate']}</span>
                        </li>
                    </ul>
                </div>
                <div
                    className="board_view_body"
                >
                    {this.props.FreeBoardDetail['body']}
                </div>
            </div>
        );

        return (<div
                className="board_view"
            >
                {
                    this.props.FreeBoardDetail&&
                    detailBoard()
                }
                {
                    this.props.Loading === 'Loading' &&
                    <CircularProgress size={60} thickness={5}/>
                }
                {
                    this.props.FreeBoardDetail &&
                    CommentLayout(this.props.FreeBoardDetail.FreeBoardComment, this.handleSubmit, this.props.putRecommend, this.props.userdata)
                }
            </div>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        FreeBoardDetail:state.notice.FreeBoardDetail,
        commentWriteSuccess : state.notice.commentWriteSuccess,
        Comment: state.AWS.comment,
        userdata : state.authentication.logindata,
        Loading: state.notice.Loading,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        boardDetail: (pk) =>{
            dispatch(boardDetail(pk));
        },
        getComments: (pk) => {
            dispatch(getComments(pk))
        },
        putComments: (author, text, password, url, book, subtitle) => {
            dispatch(putComments(author, text, password, url, book, subtitle))
        },
        putRecommend: (pk,like) =>{
            dispatch(recommend(pk, like))
        },
        clearComments:() => {
            dispatch(clearComments())
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(BoardDetail);