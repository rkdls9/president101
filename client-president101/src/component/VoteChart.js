/**
 * Created by thesky on 2017-02-20.
 */
import React, {Component} from 'react';
import {getPresident, getUserData, logOut, getThisTime} from '../actions/DB'
import {connect} from 'react-redux';
import * as c3 from 'c3';
import CircularProgress from 'material-ui/CircularProgress'
import Contact from './Contact';

class VoteChart extends Component {

    componentDidMount() {
        this.props.getThisTime();
    }


    componentWillReceiveProps(nextProps) {
        console.log('this.props, nextProps', this.props, nextProps);
        if (this.props !== nextProps && (nextProps.ThisTimePresident.length !== 0)) {
            this._renderChart(nextProps.ThisTimePresident)
        }
    }

    _renderChart(ThisTimePresident) {
        const nameArray = [];
        const countArray = [];
        const data = [];
        ThisTimePresident.forEach((val, i) => {
            nameArray.push(val.nameFor);
            countArray.push(val.like)
        });
        data.push(nameArray);
        data.push(countArray);
        c3.generate({
                bindto: '#chart1',
                data: {
                    rows: data,
                    type: 'bar',
                },
            }
        );
        c3.generate({
                bindto: '#chart2',
                data: {
                    rows: data,
                    type: 'pie',
                },
            }
        );
        let presidentLike = [];

        ThisTimePresident.forEach((val, i) => {
            presidentLike.push(val.users.male);
        });
        // dd.push(nameArray);
        data.pop();
        data.push(presidentLike);
        c3.generate({
                bindto: '#chart3',
                data: {
                    rows: data,
                    type: 'donut'
                }, donut: {
                    title: '남성'
                }
            }
        );
        const presidentLike2 = [];
        ThisTimePresident.forEach((val, i) => {
            presidentLike2.push(val.users.female);
        });
        data.pop();
        data.push(presidentLike2);
        c3.generate({
                bindto: '#chart4',
                data: {
                    rows: data,
                    type: 'donut'
                }, donut: {
                    title: '여성'
                }
            }
        )
    }

    render() {
        console.log('VoteChart, this.props', this.props);
        const date = new Date();
        const weekth = parseInt((6 + date.getDate() - date.getDay()) / 7) + 1;
        const then = new Date("march 13, 2017");
        let dday = then.getTime() - date.getTime();
        dday = Math.floor(dday / (1000 * 60 * 60 * 24)) + 1;
        return (
            <div>
                {
                    this.props.Loading === 'Loading' &&
                    <CircularProgress
                        className="col-lg-12 col-md-12 col-sm-12 col-xs-12 LoadingCircle"
                        size={60}
                        thickness={5}/>
                }
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div>3월 13일 까지 <h1>D - {dday}일</h1></div>
                    <div>{date.getMonth() + 1}월 {weekth}째주</div>
                </div>
                <div className="col-xs-6 col-lg-3">
                    <div id="chart1"></div>
                </div>
                <div className="col-xs-6 col-lg-3">
                    <div id="chart2"></div>
                </div>
                <div className="col-xs-6 col-lg-3">
                    <div id="chart3"></div>
                </div>
                <div className="col-xs-6 col-lg-3">
                    <div id="chart4"></div>
                </div>
            </div>
        )
    }
}

const propTypes = {};
const defaultProps = {};
VoteChart.propTypes = propTypes;
VoteChart.defaultProps = defaultProps;


const mapStateToProps = (state, ownProps) => {
    return {
        ThisTimePresident: state.President.ThisTimePresident,
        Loading: state.notice.Loading,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getUserData: () => {
            dispatch(getUserData())
        },
        logOut: () => {
            dispatch(logOut())
        },
        getPresident: () => {
            dispatch(getPresident())
        },
        getThisTime: () => {
            dispatch(getThisTime())
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(VoteChart);