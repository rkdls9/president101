/**
 * Created by youngil on 2017-02-15.
 */

import {connect} from 'react-redux';
import React, {Component} from 'react';
import {boardList, writingBoard, clearWritingProps} from '../actions/DB'
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton';
import {Link, Redirect} from 'react-router-dom';


class Writing extends Component {

    constructor(props) {
        super(props);
        this.state = {
            multiline: '',
            title: '',
            finished:false
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.beforeBoardList = this.beforeBoardList.bind(this)
    }

    componentDidMount() {

    }

    componentWillUnmount() {
        this.props.clearWritingProps()
    }

    onSubmit() {
        console.log('on submit called', this.state);
        this.props.writingBoard(this.state.title, this.state.multiline, this.props.logindata.username);
        this.setState({
            multiline: '',
            title: '',
            finished:true
        });
        window.scrollTo(0, 0)
    }

    beforeBoardList(){
        this.setState({
            finished:false,
        });
        this.props.boardList('page1', 1)
    }

    handleChange(e) {
        let nextState = {};
        nextState[e.target.name] = e.target.value;
        this.setState(nextState);
    }

    render() {
        console.log('Writing ', this.props);
        console.log('Writing ', this.state);
        return (<div
                className="WriteForm"
            >
                <TextField
                    floatingLabelText={`아이디`}
                    value={this.props.logindata.username}
                    disabled={true}/>
                <br/>
                <TextField
                    fullWidth={true}
                    value={this.state.title}
                    floatingLabelText={"제목"}
                    name="title"
                    onChange={this.handleChange.bind(this)}
                />
                <br/>
                <TextField
                    fullWidth={true}
                    value={this.state.multiline}
                    multiLine={true}
                    rows={3}
                    name="multiline"
                    floatingLabelText={"내용"}
                    onChange={this.handleChange.bind(this)}
                    rowsMax={10}
                />
                <div>
                    <center>
                        <RaisedButton
                            onClick={this.onSubmit}
                            label="확인"/>
                        <Link
                            to="/"
                        ><RaisedButton label="취소"/></Link>
                    </center>
                </div>
            </div>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        logindata: state.authentication.logindata,
        writeStatus: state.notice.writeStatus,
        writedPk: state.notice.writedPk
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        writingBoard: (title, text, id) => {
            dispatch(writingBoard(title, text, id))
        },
        clearWritingProps: () => {
            dispatch(clearWritingProps())
        },
        boardList: (url, page) => {
            dispatch(boardList(url, page))
        },
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Writing);