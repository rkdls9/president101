/**
 * Created by youngil on 2017-02-07.
 */
import React, {Component} from 'react';
import {Route} from 'react-router-dom';
import {getPresident, getUserData, logOut, getThisTime} from '../actions/DB'
import {connect} from 'react-redux';
import PresidentList from './PresidentList'
import NaviBar from './NaviBar'

import ListMessageWithoutLogin from './ListMessageWithoutLogin'
import Login from './Login'
// import VoteChart from './VoteChart';
import VotechartReload from './VoteChartReload';
import Mypage from './Mypage'
import Rank from './Rank';
import LoginAfter from './LoginAfter'
import About from './About'
const Scroll = require('react-scroll');
const SLink = Scroll.Link;
const Events = Scroll.Events;
const Element = Scroll.Element;
const scrollSpy = Scroll.scrollSpy;

class Contents extends Component {

    componentDidMount() {
        this.props.getUserData();
        this.props.getPresident();

    }

    render() {
        console.log('home, this.props', this.props);
        return (<Element name="home">
            <div
                className="container-fluid rootcontents"
            >
                <Route path='/' component={NaviBar}
                       username={this.props.userdata.username}
                />
                <Route path='/' exact={true} component={About}/>
                <Route path='/' exact={true} component={PresidentList}/>
                <Route path='/' exact={true} component={VotechartReload}/>
                <Route path="/" exact={true} component={ListMessageWithoutLogin}/>
                <Route path="/_=_" exact={true} component={LoginAfter}/>
                <Route path="/rank" component={Rank}/>
                {/*<Route path="/board/" component={Board}/>*/}
                {/*<Route path="/board/" component={ListMessage}/>*/}
                <Route path="/mypage" component={Mypage}/>
                {/*<Route path="/contact" component={Contact}/>*/}
            </div>
            </Element>
        )
    }
}

const propTypes = {};
const defaultProps = {};
Contents.propTypes = propTypes;
Contents.defaultProps = defaultProps;


const mapStateToProps = (state, ownProps) => {
    return {
        status: state.authentication.login.status,
        userdata: state.authentication.logindata
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getUserData: () => {
            dispatch(getUserData())
        },
        logOut: () => {
            dispatch(logOut())
        },
        getPresident: () => {
            dispatch(getPresident())
        },
        getThisTime: () => {
            dispatch(getThisTime())
        }

    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Contents);