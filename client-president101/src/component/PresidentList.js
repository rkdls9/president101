/**
 * Created by youngil on 2017-02-09.
 */
import naturalSort from 'javascript-natural-sort';
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {votePresident} from '../actions/DB'
import CircularProgress from 'material-ui/CircularProgress'
import RaisedButton from 'material-ui/RaisedButton';
import Snackbar from 'material-ui/Snackbar';
const Scroll = require('react-scroll');
const sLink = Scroll.Link;
const events = Scroll.Events;
const Element = Scroll.Element;


const make_president_list = (val, imgsrc, onRadioChange) => {
    return <div
        key={val.pk.toString()}
        className="presidentList col-lg-3 col-xs-12 col-md-3 col-sm-4"
    >
        <center>
            <label htmlFor={val.Name}>
                <img
                    className="presidentImage"
                    src={imgsrc} alt="president"
                />
                <li><b>{val.Name}</b></li>
                <li>나이:{val.age}</li>
                <li>당:{val.belongtobe}</li>
                <li>가족:{val.familly}</li>
                <li>고향:{val.hometown}</li>
                <li>학력:{val.school}</li>
                <li>
                    <input type="radio" name="president" id={val.Name} value={val.pk}
                           onChange={onRadioChange}
                    />
                    <label htmlFor={val.Name}> {val.Name}</label>
                </li>
            </label>
        </center>
    </div>
};
const make_no_presidnet = (val, imgsrc, onRadioChange) => {
    return <div
        key={val.pk.toString()}
        className="presidentList col-lg-3 col-xs-12 col-md-3 col-sm-4"
    >
        <center>
            <label htmlFor={val.Name}>
                <img
                    className="presidentImage"
                    src={imgsrc} alt="president"/>
                <li><b>{val.Name}</b></li>
                <li>나는 기권</li>
                <li>
                    <input type="radio" name="president" id={val.Name} value={val.pk}
                           onChange={onRadioChange}
                    />
                    <label htmlFor={val.Name}> {val.Name}</label>
                </li>
            </label>
        </center>
    </div>
};
class PresidentList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            president: '',
            open: false,
            message: '투표되었습니다',
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onRadioChange = this.onRadioChange.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();
        if (this.state.president) {
            this.props.votePresident(this.state.president)
        }
    }

    componentWillReceiveProps(nextProps) {
        if (this.props !== nextProps) {
            console.log('voting res ', this.props, nextProps);
            if (typeof nextProps.VotingRes !== 'undefined' && nextProps.VotingRes.length !== 0) {
                this.setState({
                    message: `${nextProps.VotingRes}`,
                    open: true
                });
            }
        }
    }

    onRadioChange(event) {
        this.setState({
            president: event.target.value
        })
    }

    render() {
        console.log('PresidentList props', this.props);
        // this.props.President.sort((a, b) =>{
        //     return naturalSort(a.Name, b.Name)
        // });
        return (<div
                className="col-lg-12 col-md-12 col-sm-12 col-xs-12"
            >
                {
                    this.props.Loading === 'Loading' &&
                    <center>
                        <CircularProgress
                            className="col-lg-12 col-md-12 col-sm-12 col-xs-12 LoadingCircle"
                            size={60}
                            thickness={5}/>
                    </center>
                }
                {
                    this.props.President && this.props.Loading !== 'Loading' &&
                    this.props.President.sort((a, b) => {
                        return (a.numbers-b.numbers)
                    }) &&
                    this.props.President.map((val, i) => {
                            let imgsrc;
                            const rand = Math.floor(Math.random() * 10);
                            let president;
                            let seq = '';
                            if (val.Name === '문재인') {
                                president = 'moon'
                            } else if (val.Name === '안철수') {
                                president = 'an'
                            } else if (val.Name === '안희정') {
                                president = 'hee'
                            } else if (val.Name === '이재명') {
                                president = 'lee'
                            } else if (val.Name === '유승민') {
                                president = 'you'
                            } else if (val.Name === '기권') {
                                president = 'jungen'
                            } else if (val.Name === '심상정') {
                                president = 'shim'
                            } else if (val.Name === '남경필') {
                                president = 'nam'
                            } else if (val.Name === '손학규') {
                                president = 'son'
                            } else if (val.Name === '정운찬') {
                                president = 'jung'
                            } else if (val.Name === '홍준표') {
                                president = 'hong'
                            } else if (val.Name === '김종인') {
                                president = 'kim'
                            } else if (val.Name === '조원진') {
                                president = '6'
                            } else if (val.Name === '오영국') {
                                president = '7'
                            } else if (val.Name === '장성민') {
                                president = '8'
                            } else if (val.Name === '이재오') {
                                president = '9'
                            } else if (val.Name === '김선동') {
                                president = '10'
                            } else if (val.Name === '남재준') {
                                president = '11'
                            } else if (val.Name === '이경희') {
                                president = '12'
                            } else if (val.Name === '김정선') {
                                president = '13'
                            } else if (val.Name === '윤홍식') {
                                president = '14'
                            } else if (val.Name === '김민찬') {
                                president = '15'
                            }
                            if (rand % 2 === 0) {
                                seq = '2';
                            } else if (president === 'jungen') {
                                seq = '';
                            }
                            {/*imgsrc = require('../images/president_'+president+seq+'.png');*/
                            }
                            imgsrc = require('../images/president_' + president + '.png');
                            if (val.Name === '기권') {
                                return make_no_presidnet(val, imgsrc, this.onRadioChange)
                            }
                            return make_president_list(val, imgsrc, this.onRadioChange)
                        }
                    )
                }
                <div>
                    <RaisedButton
                        fullWidth={true}
                        secondary={true}
                        className="voteButton"
                        label="당신의 대통령에게 투표하세요!"
                        onClick={this.handleSubmit}
                    />
                    <Snackbar
                        open={this.state.open}
                        message={this.state.message}
                        autoHideDuration={3000}
                    />
                </div>
            </div>
        )
    }
}
const propTypes = {};
const defaultProps = {};
PresidentList.propTypes = propTypes;
PresidentList.defaultProps = defaultProps;
const mapStateToProps = (state, ownProps) => {
    return {
        President: state.President.President,
        VotingRes: state.President.VotingRes,
        Loading: state.notice.Loading
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        votePresident: (president) => {
            dispatch(votePresident(president))
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(PresidentList);