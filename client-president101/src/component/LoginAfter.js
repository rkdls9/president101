import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Redirect } from 'react-router-dom';

class LoginAfter extends Component {

    componentDidMount() {
    }

    render() {
        console.log('Login After Props ', this.props);

        return (
            <div>
                <Redirect push to='/'/>
            </div>
        );
    }
}

const propTypes = {};
const defaultProps = {};
LoginAfter.propTypes = propTypes;
LoginAfter.defaultProps = defaultProps;

const mapStateToProps = (state, ownProps) => {
    return {
    }
};

const mapDispatchToProps = (dispatch) => {
    return {

    }
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginAfter);