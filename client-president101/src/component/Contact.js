/**
 * Created by youngil on 2017-03-06.
 */
import React, {Component} from 'react';

class Contact extends Component {
    render() {
        console.log('Contact Us', this.props);
        return (<div
                className="col-xs-12 col-lg-12 contactProfile"
            >
                <div>
                    <a
                        className="eachProfile"
                        href="https://www.facebook.com/profile.php?id=100004031719898">
                        <img src="https://graph.facebook.com/100004031719898/picture?type=small"
                             alt="프로필"/>
                    </a>
                </div>
                <div>
                    <a
                        className="eachProfile"
                        href="https://www.facebook.com/profile.php?id=100003196449075">
                        <img src="https://graph.facebook.com/100003196449075/picture?type=small"
                             alt="디자이너프로필"
                        />
                    </a>
                </div>
            </div>
        )
    }
}
export default Contact;