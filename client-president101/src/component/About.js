/**
 * Created by youngil on 2017-03-09.
 */
import React, {Component} from 'react';
const Scroll = require('react-scroll');
const Element = Scroll.Element;

class About extends Component {
    render() {
        console.log('About', this.props);
        return (
            <Element name="about">
            <div
                className="col-xs-12 col-lg-12 contactProfile"
            >
                <center>
                    <h1>
                        드디어 시작된 대권전쟁</h1>
                    <h1>
                        매주 투표를 통해서 각 후보들의 지지율을 확인합니다
                    </h1>
                    <h3>
                        투표는 매주 한번만 가능합니다</h3>
                    <h3>
                        한번투표하면 다음주에 다시 투표가 가능합니다
                    </h3>
                    <h3>
                        로그인을 하고 투표를 하시면 투표결과를 보실수있습니다.
                    </h3>
                    <h5>
                        각투표는 매주 월요일(일요일밤 12시)에 갱신됩니다
                    </h5>
                    <h5>
                        중복투표를 방지하기위해서 페이스북 로그인만 가능합니다
                    </h5>
                </center>
            </div>
            </Element>
        )
    }
}
export default About;