/**
 * Created by thesky on 2017-02-20.
 */
import React, {Component} from 'react';
import {getPresident, getUserData, logOut, getThisTime} from '../actions/DB'
import {connect} from 'react-redux';
import * as c3 from 'c3';
import * as d3 from 'd3';
import CircularProgress from 'material-ui/CircularProgress'
import Login from './Login'
import Contact from './Contact';


class VoteChartReload extends Component {

    constructor(props) {
        super(props);
        this.state = {
            char1: '',
            char2: '',
            seq: false
        };
    }

    componentDidMount() {
        this.props.getThisTime();
        console.log('did Mounteddd')
    }

    componentWillUnmount() {
        console.log('unmount', this.intervalId1, this.intervalId2);
        clearInterval(this.intervalId1);
        clearInterval(this.intervalId2);
    }

    componentWillReceiveProps(nextProps) {
        console.log('voteChartReload this.props, nextProps', this.props, nextProps);
        if ((this.props.userdata.username !== nextProps.userdata.username) && nextProps.userdata.username) {
            this.props.getThisTime();
        }
        if (this.props.VOTERESULT && !this.state.seq && this.props !== nextProps && (nextProps.ThisTimePresident.length !== 0)) {
        // if (!this.state.seq && this.props !== nextProps && (nextProps.ThisTimePresident.length !== 0)) {
            console.log('render startttt');
            this._renderChart(nextProps.ThisTimePresident)
        }
    }

    _renderChart(ThisTimePresident) {
        const nameArray = [];
        const countArray = [];
        const data = [];
        ThisTimePresident.forEach((val, i) => {
            nameArray.push(val.nameFor);
            countArray.push(val.like)
        });
        data.push(nameArray);
        data.push(countArray);
        let chart1 = c3.generate({
                bindto: '#chart1',
                data: {
                    rows: data,
                    type: 'bar',
                },
            }
        );
        let trigger = true;
        this.intervalId1 = setInterval(function () {
            if (trigger) {
                chart1.transform('pie');
                trigger = !trigger
            } else {
                chart1.transform('bar');
                trigger = !trigger
            }
        }, 7000);

        let presidentLike = [];
        const genderdata = [];
        ThisTimePresident.forEach((val, i) => {
            presidentLike.push(val.users.male);
        });
        genderdata.push(nameArray);
        genderdata.push(presidentLike);
        const chart2 = c3.generate({
                bindto: '#chart2',
                data: {
                    rows: genderdata,
                    type: 'donut'
                }, donut: {
                    title: '남성'
                }
            }
        );
        const presidentLike2 = [];

        const dada = [];
        ThisTimePresident.forEach((val, i) => {
            presidentLike2.push(val.users.female);
        });
        dada.push(nameArray);
        dada.push(presidentLike2);

        let secondTrigger = true;
        this.intervalId2 = setInterval(function () {
            if (secondTrigger) {
                d3.select('#chart2 .c3-chart-arcs-title').node().innerHTML = '여성';
                chart2.load({
                        rows: dada,
                    },
                );
                secondTrigger = !secondTrigger;
            } else {
                d3.select('#chart2 .c3-chart-arcs-title').node().innerHTML = '남성';
                chart2.load({
                        rows: genderdata,
                    },
                );
                secondTrigger = !secondTrigger;
            }
        }, 7000);
        this.setState({
            seq: true
        })
    }

    render() {
        console.log('VoteChart, this.props', this.props);
        const date = new Date();
        const weekth = parseInt((6 + date.getDate() - date.getDay()) / 7) + 1;
        const then = new Date("may 9, 2017");
        let dday = then.getTime() - date.getTime();
        dday = Math.floor(dday / (1000 * 60 * 60 * 24)) + 1;
        return (
            <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                {
                    this.props.Loading === 'Loading' &&
                    <center>
                        <CircularProgress
                            className="col-lg-12 col-md-12 col-sm-12 col-xs-12 LoadingCircle"
                            size={60}
                            thickness={5}/>
                    </center>
                }
                <div className="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div
                        className="ddayconf"
                    >최종미션 5월 9일 까지 <h1>D - {dday}일</h1></div>
                    <div>{date.getMonth() + 1}월 {weekth}째주</div>
                </div>
                {
                    !this.props.userdata.username &&
                    <Login/>
                }

                {/*{*/}
                    {/*this.props.VOTERESULT && this.props.ThisTimePresident.length!==0&&*/}
                    {/*<div>다음 업데이트 시간 {this.props.ThisTimePresident[0].nextUpdate}</div>*/}
                {/*}*/}
                {
                    this.props.VOTERESULT &&
                    <div>
                        <div className="col-xs-12 col-lg-6 col-sm-6">
                            <div id="chart1"></div>
                        </div>
                        <div className="col-xs-12 col-lg-6 col-sm-6">
                            <div id="chart2"></div>
                        </div>
                    </div>
                }

                {/*<div>*/}
                    {/*<div className="col-xs-12 col-lg-6 col-sm-6">*/}
                        {/*<div id="chart1"></div>*/}
                    {/*</div>*/}
                    {/*<div className="col-xs-12 col-lg-6 col-sm-6">*/}
                        {/*<div id="chart2"></div>*/}
                    {/*</div>*/}
                {/*</div>*/}

                {
                    !this.props.VOTERESULT &&
                    <div>
                        <center><h1>투표하시면 투표결과를 보실수있어요!</h1></center>
                    </div>
                }
            </div>
        )
    }
}

const propTypes = {};
const defaultProps = {};
VoteChartReload.propTypes = propTypes;
VoteChartReload.defaultProps = defaultProps;


const mapStateToProps = (state, ownProps) => {
    return {
        ThisTimePresident: state.President.ThisTimePresident,
        Loading: state.notice.Loading,
        VOTERESULT: state.President.VOTERESULT,
        userdata: state.authentication.logindata
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getUserData: () => {
            dispatch(getUserData())
        },
        logOut: () => {
            dispatch(logOut())
        },
        getPresident: () => {
            dispatch(getPresident())
        },
        getThisTime: () => {
            dispatch(getThisTime())
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(VoteChartReload);