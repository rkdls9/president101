/**
 * Created by thesky on 2017-02-27.
 */

import {connect} from 'react-redux';
import React, {Component} from 'react';
import {boardList, searchBoard, searchBoardList} from '../actions/DB'
import {List, ListItem} from 'material-ui/List'
import RaisedButton from 'material-ui/RaisedButton';
import {Link, Route, Switch} from 'react-router-dom';
import Writing from './Writing'
import BoardDetail from './BoardDetail'
import CircularProgress from 'material-ui/CircularProgress'
import TextField from 'material-ui/TextField'

class ListMessage extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchText: ''
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this)
    }

    handleChange(e) {
        let nextState = {};
        nextState[e.target.name] = e.target.value;
        this.setState(nextState);
    }
    handleKeyDown(e){
        console.log('eeee',e);
        if(e.keyCode === 13) {
            this.onSubmit()
        }
    }


    componentDidMount() {
        const {location} = this.props;
        console.log('propsdd', this.props);
        if (!location.search) {
            this.props.boardList('page1', 1)
        }
        else if (location.state.page) {
            console.log('locations.state.page', location.state.page);
            this.props.boardList('/board/boardList/', location.state.page.i)
        }
    }

    onSubmit() {
        console.log('on submit called', this.state);
        this.props.searchBoard(this.state.searchText);
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.location.search !== nextProps.location.search) {
            console.log('locations.state.page', this.props.location, nextProps.location);
            if (typeof nextProps.location.state !== 'undefined' && nextProps.location.search.includes('search')){
                const searchkeyword = nextProps.location.search.split('&')[1].split('=')[1];
                this.props.searchBoardList('/board/boardList/', nextProps.location.state.page.i,  searchkeyword);
            }
            else if (typeof nextProps.location.state !== 'undefined') {
                this.props.boardList('/board/boardList/', nextProps.location.state.page.i)
            }
        }
    }

    render() {
        console.log('BoardList Props', this.props);
        console.log('Pagination', this.props.Pagination.count);
        console.log('Pagination/10', Math.ceil(this.props.Pagination.count / 10));
        const pageRange = [];
        const page_range = (count) => {


            if (this.props.location.search ) {
                console.log('this.props.locations.search', this.props.location.search);
            }

            for (let i = 1; i <= Math.ceil(count / 10); i++) {
                let searchparam = `?page=${i}`;
                if (typeof this.props.location.search !== 'undefined') {
                    console.log('this.props.location.search', this.props.location.search);
                }
                if (typeof this.props.location.search !== 'undefined' && this.props.location.search.includes('search')) {
                    const searchkeyword = this.props.location.search.split('&')[1].split('=')[1];
                    console.log('search keyword', searchkeyword);
                    console.log('this.props.location.search', `?page=${i}&search=${searchkeyword}`);
                    searchparam = `?page=${i}&search=${searchkeyword}`;
                }
                pageRange.push(<li
                    key={i.toString()}
                ><Link
                    to={{
                        pathname: '/board/',
                        search: `${searchparam}`,
                        state: {page: {i}},
                    }}
                >{i}
                </Link></li>)
            }
        };
        return (<div>
                <Switch>
                    <Route
                        exact={true}
                        path="/board/write" component={Writing}
                    />
                    <Route
                        exact={true}
                        path="/board/:pk" component={BoardDetail}
                    />
                </Switch>
                {
                    this.props.Loading === 'Loading' &&
                    <CircularProgress size={60} thickness={5}/>
                }

                <List
                    className="datalist"
                >
                    {
                        this.props.FreeBoard &&
                        this.props.FreeBoard.map((val, i) => {
                            const reg_date = val.regdate.split('T');
                            const valid_date = reg_date[0].slice(5,) + ' ' + reg_date[1].slice(0, -10);
                            {/*const primary = val.title + (val.FreeBoardComment.length === 0 ? '' : `[${val.FreeBoardComment.length}]`) + `   by ${val.author}`;*/}
                            return <Link to={`/board/${val.pk}`}
                                         key={val.pk}
                            >
                                <ListItem
                                    primaryText={primaryText(val.title, val.author, val.FreeBoardComment, val.views, valid_date)}
                                    secondaryText={val.body}
                                    secondaryTextLines={2}
                                />
                            </Link>
                        })
                    }
                </List>
                <div className="text-center">
                    <ul className="pagination">
                        {
                            this.props.Pagination &&
                            page_range(this.props.Pagination.count)
                        }
                        {
                            pageRange.map((val, i) => (
                                    val
                                )
                            )
                        }
                    </ul>
                </div>
                < div >
                    {
                        this.props.userdata.username &&
                        <Link to="/board/write"><RaisedButton label="글쓰기"/></Link>
                    }
                    <center>
                        <TextField
                            value={this.state.searchText}
                            multiLine={false}
                            rows={1}
                            name="searchText"
                            floatingLabelText={"제목+내용"}
                            onChange={(e)=>{
                                this.handleChange(e)
                            }}
                            onKeyDown={this.handleKeyDown}
                        />
                        <Link to={{
                            pathname: `/board/`,
                            search: `?page=1&search=${this.state.searchText}`
                        }}
                              onClick={this.onSubmit}
                        >
                            <RaisedButton
                                label="검색"
                            />
                        </Link>
                    </center>
                </div>

            </div>
        );
    }
}
const primaryText = (title, author, comment, views, date) => (
    <div>
        <span>{title} {comment.length === 0 ? '' : `[${comment.length}]`}</span>
        <span className="board_view_author"> by {author} </span>
        <span className="etc">{date}</span>
        <em className="etc">|</em>
        <span className="etc">Hit {views}</span>
    </div>
);
const mapStateToProps = (state, ownProps) => {
    return {
        userdata: state.authentication.logindata,
        FreeBoard: state.notice.FreeBoard,
        Pagination: state.notice.Pagination,
        Loading: state.notice.Loading,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        boardList: (url, page) => {
            dispatch(boardList(url, page))
        },
        searchBoard: (text) => {
            dispatch(searchBoard(text))
        },
        searchBoardList: (url, page, keyword) =>{
            dispatch(searchBoardList(url, page, keyword))
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(ListMessage);