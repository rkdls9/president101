/**
 * Created by youngil on 2017-02-07.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {defaulturl, loginRequest, getUserData , logOut, LoginReq} from '../actions/DB'
const Scroll = require('react-scroll');
const Element = Scroll.Element;


class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            id: "",
            password: "",
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleLogin = this.handleLogin.bind(this);
    }

    componentDidMount() {
    }
    handleChange(e) {
        let nextState = {};
        nextState[e.target.name] = e.target.value;
        this.setState(nextState);
    }



    handleLogin(e) {
        e.preventDefault();
        let id = this.state.id;
        let pw = this.state.password;
        this.props.loginRequest(id, pw)
    }



    render() {
        console.log('Login Props ', this.props);

        const loginURL=`${defaulturl}/accounts/facebook/login/`;
        return (
            <center>
                <Element name="login">
                <div
                    className="social-wrap a"
                ><a href={loginURL}>
                    <button
                        className="facebookbt"
                    >
                        페이스북로그인
                    </button>
                    </a>
                </div>
                </Element>
            </center>
        );
    }
}

const propTypes = {};
const defaultProps = {};
Login.propTypes = propTypes;
Login.defaultProps = defaultProps;

const mapStateToProps = (state, ownProps) => {
    return {
        // status: state.authentication.login.status,
        // userdata: state.authentication.status
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        loginRequest: (username, password) => {
            dispatch(loginRequest(username, password))
        },
        getUserData : () =>{
            dispatch(getUserData())
        },
        LoginReq: () =>{
            dispatch(LoginReq())
        },
        logOut : () => {
            dispatch(logOut())
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);