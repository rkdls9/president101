/**
 * Created by thesky on 2017-02-27.
 */

import {connect} from 'react-redux';
import React, {Component} from 'react';
import {boardList, searchBoard, searchBoardList, boardListMore} from '../actions/DB'
import {List, ListItem} from 'material-ui/List'
import RaisedButton from 'material-ui/RaisedButton';
import {Link, Route, Switch} from 'react-router-dom';
import Writing from './Writing'
import CircularProgress from 'material-ui/CircularProgress'
import TextField from 'material-ui/TextField'
const Scroll = require('react-scroll');
const sLink = Scroll.Link;
const events = Scroll.Events;
const Element = Scroll.Element;


const primaryText = (pk, title, author, date) => (

    <div>
        <span className="primaryKeypk"><small>{pk}</small></span>
        <span><b>{title}</b></span>
        <span className="board_view_author"> by {author} </span>
        <span className="etc">{date}</span>
    </div>
);
class ListMessageWithoutLogin extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchText: '',
            bbs: [],

            search: false,
            searched: false
        };
        this.onSubmit = this.onSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleKeyDown = this.handleKeyDown.bind(this);
        this.moreResult = this.moreResult.bind(this);
    }

    moreResult() {
        if (this.props.Pagination.next) {
            this.props.boardListMore(this.props.Pagination.next);
        }
    }

    handleChange(e) {
        let nextState = {};
        nextState[e.target.name] = e.target.value;
        this.setState(nextState);
    }

    handleKeyDown(e) {
        if (e.keyCode === 13) {
            this.onSubmit()
        }
    }

    componentDidMount() {
        const {location} = this.props;
        console.log('propsdd', this.props);
        if (!location.search) {
            this.props.boardList('page1', 1)
        }
        else if (location.state.page) {
            console.log('locations.state.page', location.state.page);
            this.props.boardList('/board/boardList/', location.state.page.i)
        }
    }

    onSubmit() {
        console.log('on submit called', this.state);
        this.props.searchBoard(this.state.searchText);
        if (this.state.searchText !== "") {
            this.setState({
                search: true,
                searched: true
            })
        } else {
            this.setState({
                search: false,
                searched: false
            })
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log('nextPropsInlist', nextProps, this.props);

        // if (nextProps.Pagination.next) {
        //     if (nextProps.Pagination.next.includes('search') && !this.props.Pagination.next.includes('search')) {
        //         console.log('서칭 시장');
        //         const emptylist = [];
        //         this.setState({
        //             bbs: nextProps.FreeBoard,
        //         });
        //         console.log('서칭후 state', this.state)
        //     }
        //     else if (this.props.Pagination !== nextProps.Pagination) {
        //         console.log('dddd', this.state);
        //         this.setState({
        //             bbs: this.state.bbs.concat(nextProps.FreeBoard)
        //         })
        //     }
        // } else if (!nextProps.Pagination.next && nextProps.FreeBoard.length !==0 ){
        //     this.setState({
        //         bbs:this.state.bbs.concat(nextProps.FreeBoard)
        //     })
        // }


        // if (!this.state.bbs.length && nextProps.FreeBoard !== ""){
        //     this.setState({
        //         bbs: nextProps.FreeBoard,
        //     })
        // }
        // if (this.state.search){
        //     console.log('서치하는거맞음');
        //     if( this.props.Pagination.next.includes('search') ){
        //             console.log('기존것이 서치하던거였음');
        //             this.setState({
        //                 bbs: this.state.bbs.concat(nextProps.FreeBoard)
        //             })
        //     }else{
        //         console.log('서치하지만 기존에 서치안햇었음');
        //         this.setState({
        //             bbs: nextProps.FreeBoard,
        //             search: false
        //         })
        //     }
        //
        // }else{
        //     console.log('서치하는거아님');
        //     this.setState({
        //         bbs: this.state.bbs.concat(nextProps.FreeBoard)
        //     })
        // }


        console.log('this.state', this.state);
        if (!this.state.bbs) {
            console.log('초기값 설정');
            this.setState({
                bbs: nextProps.FreeBoard,
                searched: false
            })
        }
        if (this.state.searched) {
            console.log('서치시작');
            this.setState({
                bbs: nextProps.FreeBoard,
                searched: false
            })
        } else if (!this.state.searched && this.props.FreeBoard !== nextProps.FreeBoard) {
            console.log('서치후 더보기 또는 그냥 더보기');
            this.setState({
                bbs: this.state.bbs.concat(nextProps.FreeBoard)
            })
        }
        if(this.props.Pagination.next){
            if (this.props.FreeBoard !== nextProps.FreeBoard && this.props.Pagination.next === nextProps.Pagination.next){
                console.log('새글 작성으로인한 스테이트 다시 셋팅');
                this.setState({
                    bbs: nextProps.FreeBoard,
                })
            }}
    }

    render() {
        console.log('BoardList Props', this.props);
        console.log('BoardList State', this.state);
        return (<div
                className="col-lg-12 col-md-12 col-sm-12 col-xs-12"
            >
                <Element name="board">
                    <List
                        className="datalist"
                    >
                        {
                            this.state.bbs &&
                            this.state.bbs.map((val, i) => {
                                const redd = new Date(val.regdate);
                                const reg_date = val.regdate.split('T');
                                const valid_date = reg_date[0].slice(5,) + ' ' + reg_date[1].slice(0, -10);
                                return <ListItem
                                    primaryText={primaryText(val.pk, val.title, val.author, valid_date)}
                                    secondaryText={val.body}
                                    secondaryTextLines={2}
                                    key={val.pk.toString() + i + val.title.toString() + val.regdate.toString()}
                                />
                            })
                        }
                    </List>
                </Element>
                <div className="text-center">
                    {
                        this.props.LoadingBoardSearch === 'LoadingBoardSearch' &&
                        <center>
                            <CircularProgress size={60} thickness={5}/>
                        </center>
                    }

                    {
                        this.props.Pagination.next &&
                        <RaisedButton
                            onClick={this.moreResult}
                        >더보기</RaisedButton>
                    }
                </div>
                <div>
                    <center>
                        <TextField
                            value={this.state.searchText}
                            multiLine={false}
                            rows={1}
                            name="searchText"
                            floatingLabelText={"제목+내용"}
                            onChange={(e) => {
                                this.handleChange(e)
                            }}
                            onKeyDown={this.handleKeyDown}
                        />
                        <RaisedButton
                            onClick={this.onSubmit}
                            label="검색"
                        />
                    </center>
                </div>
                <Element name="write">
                    {
                        this.props.userdata.username &&
                        <Writing/>

                    }
                    {
                        !this.props.userdata.username &&
                        <div>
                            <center><h1>로그인후 글을쓰실수있습니다.</h1></center>
                        </div>
                    }
                </Element>

            </div>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        userdata: state.authentication.logindata,
        FreeBoard: state.notice.FreeBoard,
        Pagination: state.notice.Pagination,
        LoadingBoardSearch: state.notice.LoadingBoardSearch,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        boardList: (url, page) => {
            dispatch(boardList(url, page))
        },
        searchBoard: (text) => {
            dispatch(searchBoard(text))
        },
        boardListMore: (link) => {
            dispatch(boardListMore(link))
        }

    }
};
export default connect(mapStateToProps, mapDispatchToProps)(ListMessageWithoutLogin);