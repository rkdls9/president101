/**
 * Created by youngil on 2017-02-13.
 */
import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import '../App.css';
import logo from '../images/president101Logo.png'
import {connect} from 'react-redux';
import {logOut} from '../actions/DB'
const Scroll = require('react-scroll');
const SLink = Scroll.Link;
const Events = Scroll.Events;
const Element = Scroll.Element;
const scrollSpy = Scroll.scrollSpy;
const scroll = Scroll.animateScroll;
import {Nav, NavbarBrand, Navbar} from 'react-bootstrap';

class NaviBar extends Component {

    constructor(props) {
        super(props);
        this.state = {};
        this.goToFirst = this.goToFirst.bind(this)
    }

    componentDidMount() {
        Events.scrollEvent.register('begin', function (to, element) {
            console.log("begin", arguments)
        });
        Events.scrollEvent.register('end', function (to, element) {
            console.log("end", arguments)
        });
        scrollSpy.update()
    }
    goToFirst() {
        // window.scrollTo(0, 0)
        scroll.scrollToTop()
    }
    render() {
        return (
            <nav className="navbar navbar-default navbar-fixed-top">
                <div className="container-fluid makeback">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1" aria-expanded="false"
                        >
                            <span className="sr-only">Toggle navigation</span>
                            <span className="icon-bar"/>
                            <span className="icon-bar"/>
                            <span className="icon-bar"/>
                        </button>
                        <SLink
                            smooth={true}
                            className="navbar-brand"
                            href=""
                            to=""
                            onClick={this.goToFirst}
                            >
                            <img alt="presidentLogo"
                                 size="30"
                                 width={30}
                                 src={logo}/>
                        </SLink>
                    </div>
                    <div className="collapse navbar-collapse" classID="bs-example-navbar-collapse-1"
                         id="bs-example-navbar-collapse-1"
                    >
                        <ul
                            className="nav navbar-nav"
                        >
                            <li><SLink smooth={true} href="" to="" onClick={this.goToFirst}>프로그램 정보</SLink></li>
                            {/*<li><Link to="/president">101 후보생</Link></li>*/}
                            {/*<li><Link to="/rank">평가순위</Link></li>*/}
                            {/*<li><Link to="/board">게시판</Link></li>*/}
                            <li><SLink smooth={true} href="" to="board">게시판</SLink></li>
                            <li><SLink smooth={true} href="" to="write">글쓰기</SLink></li>
                            {/*<li><Link to="/report">제보하기</Link></li>*/}

                        </ul>
                        <ul
                            className="nav navbar-nav navbar-right"
                        >
                            {
                                (this.props.username === 'init' || this.props.username.length === 0) &&
                                <li><SLink smooth={true} href="" to="login">로그인</SLink></li>
                            }
                            {
                                this.props.username &&
                                <li><Link to="/">{this.props.username}</Link></li>
                            }
                            {
                                this.props.username &&
                                <li><a href="/" onClick={this.props.logOut}>로그아웃 </a></li>
                            }
                        </ul>
                    </div>
                </div>
            </nav>
        );
    }
}
const propTypes = {};
const defaultProps = {};
NaviBar.propTypes = propTypes;
NaviBar.defaultProps = defaultProps;

const mapDispatchToProps = (dispatch) => {
    return {
        logOut: () => {
            dispatch(logOut())
        }
    }
};

export default connect(null, mapDispatchToProps)(NaviBar);