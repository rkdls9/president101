/**
 * Created by youngil on 2017-02-27.
 */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {logOut, mypage} from '../actions/DB'
import CircularProgress from 'material-ui/CircularProgress'
import * as c3 from 'c3';

class Mypage extends Component {

    componentDidMount() {
        this.props.mypage()
    }

    componentWillReceiveProps(nextProps) {
        console.log('this.props, nextProps', this.props, nextProps);
        if (this.props !== nextProps && (nextProps.MyPage !== 0)) {
            this._renderChart(nextProps.MyPage, nextProps.President)
        }
    }

    _renderChart(MyPage, President) {
        const dd2 = [];
        const presidentLike2 = [];
        const nameArray = [];

        // const month = val.thisTime.slice(0, -1);
        // const weekth = val.thisTime.slice(-1, val.thisTime.length);
        President.forEach((val, i) => {
            nameArray.push(val.Name);
        });
        let hee=0;
        let an=0;
        let moon=0;
        let lee=0;
        let you=0;
        MyPage.forEach((val, i) => {
            if(val.nameFor==='안희정'){
                hee+=1
            }else if (val.nameFor ==='안철수'){
                an +=1
            }
        });
        // presidentLike2.push(hee);
        // presidentLike2.push(an);
        // presidentLike2.push(moon);
        // presidentLike2.push(lee);
        // presidentLike2.push(you);
        dd2.push(nameArray);
        dd2.push(presidentLike2);
        c3.generate({
                bindto: '#chart1',
                data: {
                    rows: dd2,
                    type: 'donut'
                }, donut: {
                }
            }
        )
    }

    render() {
        console.log('Mypage', this.props);
        return (<div>
                내가 투표한 사람들
                {
                    this.props.Loading === 'Loading' &&
                    <CircularProgress
                        className="col-lg-12 col-md-12 col-sm-12 col-xs-12 LoadingCircle"
                        size={60}
                        thickness={5}/>
                }
                {
                    this.props.MyPage && this.props.Loading !== 'Loading'&&
                        this.props.MyPage.map((val,i)=>{
                        return <div
                            key={val.thisTime.toString()}
                        >
                            <li>{val.thisTime}회차 {val.nameFor}</li>

                        </div>
                    })
                }
                {/*<div className="col-xs-6">*/}
                    {/*<div id="chart1"></div>*/}
                {/*</div>*/}
                <li><a href="/" onClick={this.props.logOut}>로그아웃</a></li>
            </div>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        MyPage: state.authentication.MyPage,
        Loading: state.notice.Loading,
        President: state.President.President,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        logOut: () => {
            dispatch(logOut())
        },
        mypage: () => {
            dispatch(mypage())
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Mypage);