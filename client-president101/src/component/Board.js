/**
 * Created by youngil on 2017-02-15.
 */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import {boardList, searchBoard} from '../actions/DB'
import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import {Link, Route, Switch} from 'react-router-dom';
import Writing from './Writing'
import BoardDetail from './BoardDetail'
import CircularProgress from 'material-ui/CircularProgress'
import TextField from 'material-ui/TextField'

class Board extends Component {

    constructor(props) {
        super(props);
        this.state = {
            searchText: ''
        };
        this.onSubmit = this.onSubmit.bind(this)
    }

    handleChange(e) {
        let nextState = {};
        nextState[e.target.name] = e.target.value;
        this.setState(nextState);
    }


    componentDidMount() {
        const {location} = this.props;
        // console.log('propsdd', this.props);
        if (!location.search) {
            this.props.boardList('page1', 1)
        }
        else if (location.state.page) {
            console.log('locations.state.page', location.state.page);
            this.props.boardList('/board/boardList/', location.state.page.i)
        }
    }

    onSubmit() {
        console.log('on submit called', this.state);
        this.props.searchBoard(this.state.searchText)
    }

    componentWillReceiveProps(nextProps) {
        if (this.props.location.search !== nextProps.location.search) {
            console.log('locations.state.page', this.props.location, nextProps.location);
            if (typeof nextProps.location.state !== 'undefined') {
                this.props.boardList('/board/boardList/', nextProps.location.state.page.i)
            }
        }
    }

    render() {
        console.log('BoardList Props', this.props);
        console.log('Pagination', this.props.Pagination.count);
        console.log('Pagination/10', Math.ceil(this.props.Pagination.count / 10));
        const pageRange = [];
        const page_range = (count) => {
            console.log('this.props.locations.search',this.props.locations.search);
            for (let i = 1; i <= Math.ceil(count / 10); i++) {
                let searchparam = `?page=${i}`;
                console.log('this.props.locations.search',this.props.locations.search);
                if (this.props.locations.search.contains('search')) {
                    console.log('this.props.locations.search',this.props.locations.search);
                    searchparam =this.props.locations.search;
                }
                pageRange.push(<li
                    key={i.toString()}
                ><Link
                    to={{
                        pathname: '/board/',
                        search: `${searchparam}`,
                        state: {page: {i}},
                    }}
                >{i}
                </Link></li>)
            }
        };
        return (<div>
                <Switch>
                    <Route
                        exact={true}
                        path="/board/write" component={Writing}
                    />
                    <Route
                        exact={true}
                        path="/board/:pk" component={BoardDetail}
                    />
                </Switch>
                {
                    this.props.Loading === 'Loading' &&
                    <CircularProgress size={60} thickness={5}/>
                }
                <Table
                    selectable={false}

                >
                    <TableHeader
                        displaySelectAll={false}
                        adjustForCheckbox={false}
                    >
                        <TableRow>
                            <TableHeaderColumn className="tableNumber col-xs-3">번호</TableHeaderColumn>
                            <TableHeaderColumn className="col-xs-7">제목</TableHeaderColumn>
                            <TableHeaderColumn className=" col-xs-2">작성자</TableHeaderColumn>
                            <TableHeaderColumn className=" col-xs-2">작성일</TableHeaderColumn>
                            <TableHeaderColumn className=" col-xs-2">조회수</TableHeaderColumn>
                        </TableRow>
                    </TableHeader>
                    <TableBody
                        displayRowCheckbox={false}
                    >
                        {
                            this.props.FreeBoard &&
                            this.props.FreeBoard.map((val, i) => {
                                const reg_date = val.regdate.split('T');
                                const valid_date = reg_date[0].slice(5,) + ' ' + reg_date[1].slice(0, -10);
                                return <TableRow key={val.pk}>
                                    <TableRowColumn className="tableNumber col-xs-3">{val.pk}</TableRowColumn>
                                    <TableRowColumn className="col-xs-7">
                                        <Link to={`/board/${val.pk}`}>
                                            <span>
                                                {val.title}
                                                <span className="titmebyComment">
                                                    {val.FreeBoardComment.length === 0 ? '' : `[${val.FreeBoardComment.length}]`}
                                                </span>
                                            </span>
                                        </Link>
                                    </TableRowColumn>
                                    <TableRowColumn className=" col-xs-2">{val.author}</TableRowColumn>
                                    <TableRowColumn className=" col-xs-2">{valid_date}</TableRowColumn>
                                    <TableRowColumn className=" col-xs-2">{val.views}</TableRowColumn>
                                </TableRow>
                            })
                        }
                    </TableBody>
                </Table>
                <div className="text-center">
                    <ul className="pagination">
                        {
                            this.props.Pagination &&
                            page_range(this.props.Pagination.count)
                        }
                        {
                            pageRange.map((val, i) => (
                                    val
                                )
                            )
                        }
                    </ul>
                </div>
                < div >
                    {
                        this.props.userdata.username &&
                        <Link to="/board/write"><RaisedButton label="글쓰기"/></Link>
                    }
                    <center>
                        <TextField
                            value={this.state.searchText}
                            multiLine={false}
                            rows={1}
                            name="searchText"
                            floatingLabelText={"제목+내용"}
                            onChange={this.handleChange.bind(this)}
                        />
                        <RaisedButton
                            onClick={this.onSubmit}
                            label="검색"/>
                    </center>
                </div>

            </div>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        userdata: state.authentication.logindata,
        FreeBoard: state.notice.FreeBoard,
        Pagination: state.notice.Pagination,
        Loading: state.notice.Loading,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        boardList: (url, page) => {
            dispatch(boardList(url, page))
        },
        searchBoard: (text) => {
            dispatch(searchBoard(text))
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Board);