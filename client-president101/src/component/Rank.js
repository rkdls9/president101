/**
 * Created by youngil on 2017-03-02.
 */
import React, {Component} from 'react';
import {getPresident, getUserData, logOut, getThisMonth} from '../actions/DB'
import {connect} from 'react-redux';
import * as c3 from 'c3';
import CircularProgress from 'material-ui/CircularProgress'

class Rank extends Component {

    componentDidMount() {
        this.props.getThisMonth();
    }


    componentWillReceiveProps(nextProps) {
        // console.log('this.props, nextProps', this.props, nextProps);
        // if (this.props !== nextProps && (nextProps.ThisMonthPresident.length !== 0)) {
            // this._renderChart(nextProps.ThisMonthPresident)
        // }
    }
    componentDidUpdate(prevProps, prevState){
        if (this.props !== prevProps && (this.props.ThisMonthPresident.length !== 0)) {
            this._renderChart(this.props.ThisMonthPresident)
        }
    };
    _renderChart(ThisMonthPresident) {
        ThisMonthPresident.forEach((val, i) => {
            const nameArray = [];
            const countArray = [];
            const male = [];
            const female = [];
            const data = [];
            val.thistimevote.forEach((vali,j) =>{
                nameArray.push(vali.nameFor);
                countArray.push(vali.like);
                male.push(vali.male);
                female.push(vali.female);
            });
            data.push(nameArray);
            data.push(countArray);
            c3.generate({
                    bindto: `#chart${i}1`,
                    data: {
                        rows: data,
                        type: 'bar',
                    },
                }
            );
            c3.generate({
                bindto: `#chart${i}2`,
                data: {
                    rows: data,
                    type: 'pie',
                },
            }
            );
            data.pop();
            data.push(male);
            c3.generate({
                bindto: `#chart${i}3`,
                data: {
                    rows: data,
                    type: 'donut'
                }, donut: {
                    title: '남성'
                }
            });
            data.pop();
            data.push(female);

            c3.generate({
                bindto: `#chart${i}4`,
                data: {
                    rows: data,
                    type: 'donut'
                }, donut: {
                    title: '여성'
                }
            }
        )

        });
    }

    render() {
        console.log('Rank, this.props', this.props);
        return (
            <div>
                {
                    this.props.Loading === 'Loading' &&
                    <CircularProgress
                        className="col-lg-12 col-md-12 col-sm-12 col-xs-12 LoadingCircle"
                        size={60}
                        thickness={5}/>
                }
                {
                    this.props.ThisMonthPresident == 0 && this.props.Loading !== 'Loading' &&
                    <div>
                    이전 기록이없습니다.
                    </div>
                }
                {
                    this.props.ThisMonthPresident &&
                    this.props.ThisMonthPresident.map((val,i) =>{
                        return <div
                            key={val.times.toString()}
                            className="col-xs-12 col-sm-12 col-md-12 col-lg-12 ranklist">
                            {makeChart(val, i)}
                        </div>
                    })
                }
            </div>
        )
    }
}

const propTypes = {};
const defaultProps = {};
Rank.propTypes = propTypes;
Rank.defaultProps = defaultProps;
const makeChart = (chart, i) => {
    return <div>
        {chart.times} 회차 총 투표수 {chart.like}
        {
            chart.thistimevote.map((val, j) =>{
                return <div
                    key={val.nameFor.toString()}
                >
                    {val.nameFor} &nbsp; : {val.like}
                </div>
            })
        }
        <div className="col-xs-6 col-lg-3">
            <div id={`chart${i}1`}/>
        </div>
        <div className="col-xs-6 col-lg-3">
            <div id={`chart${i}2`}/>
        </div>
        <div className="col-xs-6 col-lg-3">
            <div id={`chart${i}3`}/>
        </div>
        <div className="col-xs-6 col-lg-3">
            <div id={`chart${i}4`}/>
        </div>
    </div>
};

const mapStateToProps = (state, ownProps) => {
    return {
        ThisMonthPresident: state.President.ThisMonthPresident,
        Loading: state.notice.Loading,
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getThisMonth: () => {
            dispatch(getThisMonth())
        }
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Rank);