/**
 * Created by youngil on 2017-01-17.
 */
import styles from '../css/comment.css'
import React, {Component} from 'react';
import naturalSort from 'javascript-natural-sort';
import TextField from 'material-ui/TextField'
import RaisedButton from 'material-ui/RaisedButton';

export const CommentLayout = (Comment, handleSubmit, putRecommend, userdata) => {
    Comment.sort((a, b) => {
        return naturalSort(b.like, a.like)
    });
    return <div className="commentBox">
        <div className="commentTitleBox">
            <div>
                <label>댓글</label>
                <small> (로그인한 사용자만 좋아/싫어 할수있습니다)</small>
            </div>
            <div>
                {CommentList(Comment, putRecommend)}
                <CommentForm
                    handleSubmit={handleSubmit}
                    userdata={userdata}
                />
            </div>
        </div>
    </div>;
};
const CommentList = (Comment, putRecommend) => (
    Comment.map((val, i) => {
            console.log("crated_date", val);
            console.log("val type", typeof val);
            // const reg_date = new Date(val.created_date).toISOString().split('T');
            const reg_date = val.created_date.split('T');
            const valid_date = reg_date[0] + ' ' + reg_date[1].slice(0, -3);
            return <div
                key={val.pk}
                className="commentBorder"
            >
                <li
                >
                    <div
                        className="commentTopBox"
                    >
                        <b>
                        <span className="authorBox">
                        {/*{i + 1} 제목 : {val.fields.title}*/}
                            {val.author}
                        </span>
                        </b>
                        <span>
                            <button
                                className="btn btn-default btn-sm"
                                type="button" onClick={
                                (e) => {
                                    e.preventDefault();
                                    putRecommend(val.pk, 'like')
                                }
                            }
                            >
                                <span className="glyphicon glyphicon-thumbs-up"/> 좋아
                                {val.like}
                            </button>
                            <button
                                className="btn btn-default btn-sm"
                                type="button" onClick={
                                (e) => {
                                    e.preventDefault();
                                    putRecommend(val.pk, 'dislike')
                                }
                            }
                            >
                                <span className="glyphicon glyphicon-thumbs-down"/>싫어
                            </button>
                            <small className="commentDate">
                                <span className="glyphicon glyphicon-time"/>
                                <time dateTime={valid_date}>
                                    {valid_date}
                                </time>
                            </small>
                        </span>
                    </div>
                    <div>
                        <span>{val.text}</span>
                    </div>
                </li>
                <br/>
            </div>
        }
    )

);
class CommentForm extends Component {

    constructor(props){
        super(props);
        this.beforeSubmit = this.beforeSubmit.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.state = {
            text: '',
        };
    }

    beforeSubmit(){
        const {handleSubmit} = this.props;
        handleSubmit(this.state.text);
        this.setState({
            text:'',
        })
    }
    handleChange(e) {
        let nextState = {};
        nextState[e.target.name] = e.target.value;
        this.setState(nextState);
    }

    render(){
        const {userdata} = this.props;
        const {handleSubmit} = this.props;


        return(
            <div
                className="commentInner"
            >
                {
                    userdata.username.length === 0 &&
                    <div
                        className="noLoginTitle"
                    >
                        로그인한 유저만 댓글/글 을 쓰실수있습니다
                    </div>
                }
                {
                    userdata.username.length !== 0 &&
                    <div
                        onSubmit={handleSubmit} method="POST">

                        <TextField classID="author"
                                   floatingLabelText={`작성자 : ${userdata.username}`}
                                   ref="author"
                                   disabled={true}
                        />
                        <div>
                            <TextField
                                floatingLabelText="로그인한 사용자만 좋아/싫어 할수있습니다 ^^"
                                multiLine={true}
                                fullWidth={true}
                                value={this.state.text}
                                rows={3}
                                rowsMax={15}
                                name="text"
                                onChange={this.handleChange}
                            />
                        </div>
                        <div>
                            <RaisedButton
                                type="submit"
                                label="등록"
                                onClick={this.beforeSubmit}
                            />
                        </div>
                    </div>
                }
            </div>
        )

    }
}
