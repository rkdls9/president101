/**
 * Created by youngil on 2017-02-14.
 */
import {connect} from 'react-redux';
import React, {Component} from 'react';
import TextField from 'material-ui/TextField'

class WriteComment extends Component {

    constructor(props) {
        super(props);
        this.state = {
            multiline: '',
            title: '',
            password: '',
        }
    }

    componentDidMount() {

    }

    handleChange(e) {
        // this.setState({
        //     multiline: event.target.value,
        //     title: event.target.value
        // });
        let nextState = {};
        nextState[e.target.name] = e.target.value;
        this.setState(nextState);
    }

    render() {
        console.log('WriteComment ', this.props);
        console.log('WriteComment ', this.state);
        return (<div
                className="WriteForm"
            >
                <TextField
                    floatingLabelText={"아이디"}
                    value={this.props.logindata.username}
                    disabled={true}/>
                <br/>
                <TextField
                    fullWidth={true}
                    value={this.state.multiline}
                    multiLine={true}
                    rows="3"
                    name="multiline"
                    floatingLabelText={"내용"}
                    onChange={this.handleChange.bind(this)}
                    rowsMax={15}
                />
            </div>
        );
    }
}
const mapStateToProps = (state, ownProps) => {
    return {
        logindata: state.authentication.logindata
    }
};

const mapDispatchToProps = (dispatch) => {
    return {}
};
export default connect(mapStateToProps, mapDispatchToProps)(WriteComment);