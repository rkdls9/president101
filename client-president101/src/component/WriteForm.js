/**
 * Created by youngil on 2017-02-07.
 */
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {FreeBoardList} from '../actions/DB'

class WriteForm extends Component {
    constructor(props) {
        super(props);
        this.handleSubmit = this.handleSubmit.bind(this)
    }

    componentDidMount() {
        this.props.FreeBoardList();
    }

    componentWillMount() {

    }

    handleSubmit(e){
        e.preventDefault();
        const url = '/FreeBoardList';
        const author = this.refs.author.value.trim();
        const title = this.refs.title.value.trim();
        const text = this.refs.text.value;
        this.props.FreeBoardList(author, text, title, this.props.pknum, url);
        this.props.FreeBoardList();
        window.location.href = '/freeboard';
        // return <Redirect to='/freeboard'/>
    }
    render() {
        console.log('WriteForm props', this.props);
        console.log('WriteForm state', this.state);
        console.log('logindata', this.props.logindata);
        return (
            <div>
                <form
                    onSubmit={this.handleSubmit}
                    method="POST">
                    <div
                        className="form-group"
                    >
                        <label htmlFor="author">작성자</label>
                        <input classID="author" className="form-control" type="text"
                               maxLength="50"
                               placeholder="작성자" ref="author"
                               value={this.props.author}
                               contentEditable="false"
                               readOnly="true"
                        />
                        <label htmlFor="author">제목</label>
                        <input classID="title" className="form-control" type="text"
                               maxLength="50"
                               placeholder="제목" ref="title"/>
                    </div>
                    <div>
                        <div
                            className="form-group"
                        ><textarea
                            className="form-control"
                            maxLength="500"
                            placeholder="내용"
                            ref="text" rows="5"/></div>
                    </div>
                    <div>
                        <input type="submit" value="등록"/>
                    </div>
                </form>
                <hr/>
            </div>
        );
    }
}

const propTypes = {};
const defaultProps = {};
WriteForm.propTypes = propTypes;
WriteForm.defaultProps = defaultProps;


const mapStateToProps = (state, ownProps) => {
    return {
        FreeBoard: state.notice.FreeBoard,
        logindata: state.authentication.logindata
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        FreeBoardList: (...args) => {
            dispatch(FreeBoardList(...args))
        },
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(WriteForm);